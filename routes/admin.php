<?php

// Resource Page Link
route::resource('/homeAdmin','HomeAdminController');

//Histocal route
//Deposits
Route::resource('/historicalDepositAdmin','Admin\HistoricalDepositAdminController');
Route::resource('/historicalDepositDoubleAdmin','HistoricalDepositDoubleAdminController');
Route::resource('/historicalDepositValidAdmin','HistoricalDepositValidAdminController');
Route::resource('/historicalDepositNonValidAdmin','HistoricalDepositNonValidAdminController');

//Withdrawal
Route::resource('/historicalWithdrawalAdmin','HistoricalWithdrawalAdminController');
Route::resource('/historicalWithdrawalValidAdmin','HistoricalWithdrawalValidAdminController');
Route::resource('/historicalWithdrawalNValidAdmin','HistoricalWithdrawalNValidAdminController');

//view Item
//Detail Deposits
Route::get('/loadhistoricalDepositAdmin','Admin\HistoricalDepositAdminController@historicalDeposite')->name('admin.historical_deposit.data');
Route::get('/loadhistoricalDepositDoubleAdmin','HistoricalDepositDoubleAdminController@historicalDeposite')->name('admin.historical_deposit_double.data');
Route::get('/loadhistoricalDepositValidAdmin','HistoricalDepositValidAdminController@historicalDeposite')->name('admin.historical_deposit_valid.data');
Route::get('/loadhistoricalDepositNonValidAdmin','HistoricalDepositNonValidAdminController@historicalDeposite')->name('admin.historical_deposit_non_valid.data');

//Detail Withdrawal
Route::get('/loadhistoricalWithdrawalAdmin','HistoricalWithdrawalAdminController@historicalWithdrawal')->name('admin.historical_withdrawal.data');
Route::get('/loadhistoricalWithdrawalNValidAdmin','HistoricalWithdrawalNValidAdminController@historicalWithdrawal')->name('admin.historical_withdrawal_non_valid.data');
Route::get('/loadhistoricalWithdrawalValidAdmin','HistoricalWithdrawalValidAdminController@historicalWithdrawal')->name('admin.historical_withdrawal_valid.data');


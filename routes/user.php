<?php

Route::resource('/homepage','HomePageController');

// Test
route::resource('/homeUser','HomeUserController');
route::resource('/balanceUser','BalanceController');
route::resource('/depositeUser','DepositeController');
route::resource('/withdrawalUser','WithdrawalController');
route::resource('/profileUser','ProfileController');

//Histocal route
Route::resource('/historicalDepositUser','HistoricalDepositeController');
Route::resource('/historicalWithdrawalUser','HistoricalWithdrawalController');


Route::get('/loadhistoricalDepositUser','HistoricalDepositeController@historicalDeposite')->name('users.historical_deposit.data');
Route::get('/loadhistoricalWithdrawalUser','HistoricalWithdrawalController@historicalWithdrawal')->name('users.historical_withdrawal.data');

<?php

Route::resource('/homepage','HomePageController');

// Resource Page Link
route::resource('/homeSAdmin','HomeSAdminController');
route::resource('/typeInvestSAdmin','TypeInvestAdminController');
route::resource('/investSAdmin','InvestAdminController');

// Resource Page Link
route::post('/settingSAdmin/update_cfa','SettingSAdminController@update_cfa')->name('settingSAdmin.update_cfa');
route::post('/settingSAdmin/update_dollar','SettingSAdminController@update_dollar')->name('settingSAdmin.update_dollar');
route::post('/settingSAdmin/update_withdrawal_cfa','SettingSAdminController@update_cfa')->name('settingSAdmin.update_withdrawal_cfa');
route::post('/settingSAdmin/update_withdrawal_dollar','SettingSAdminController@update_dollar')->name('settingSAdmin.update_withdrawal_dollar');

// Link for associate Type Investiment PACK
route::post('/typeInvestSAdmin/add_type_invest_pack','TypeInvestAdminController@add_type_invest_pack')->name('typeInvest.add_type_invest_pack');
route::get('/typeInvestSAdmin2/historical_type_invest_pack','TypeInvestAdminController@historicalTypeInvest')->name('sadmin.typeInvest.historicalTypeInvest');

// Link associate for Investiment Pack
route::post('/investSAdmin/add_invest_pack','InvestAdminController@add_type_invest_pack')->name('sadmin.invest.add_type_invest_pack');
route::get('/investSAdmin2/historical_type_invest_pack','InvestAdminController@historicalTypeInvest')->name('sadmin.invest.historicalTypeInvest');

route::resource('/settingSAdmin','SettingSAdminController');

/*route::resource('/balanceAdmin','BalanceController');
route::resource('/depositeAdmin','DepositeController');
route::resource('/withdrawalAdmin','WithdrawalController');
route::resource('/profileAdmin','ProfileController');*/

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['ismadmin'], 'namespace' => 'Admin' ], function(){
	require_once __DIR__ . '/admin.php';
});


//Groupe des pages du Super Admin
Route::group(['middleware' => ['ismsuperadmin'], 'namespace' => 'SuperAdmin' ], function(){
	require_once __DIR__ . '/superadmin.php';
});


Route::group(['middleware' => ['ismuser'], 'namespace' => 'User' ], function(){
	require_once __DIR__ . '/user.php';
});


	require_once __DIR__ . '/guest.php';


<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInformationSystemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information_systemes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('nombre_pers')->default(0);
            $table->biginteger('nombre_invest')->default(0);
            $table->biginteger('nombre_deposit')->default(0);
            $table->biginteger('nombre_withdrawal')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('information_systemes');
    }
}

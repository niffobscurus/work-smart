<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('id_user')->unsigned();
            $table->biginteger('accound');
            $table->string('user',25);
            $table->string('ticket');
            $table->string('etat');
            $table->integer('number_invest')->default('1');
            $table->boolean('statut')->default('0');
            $table->dateTime('date_end')->nullable();
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invests');
    }
}

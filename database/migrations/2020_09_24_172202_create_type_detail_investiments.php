<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTypeDetailInvestiment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_detail_investiment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user');
            $table->string('libelle');
            $table->text('description')->nullable();
            $table->string('value_one')->nullable();
            $table->string('etat')->default('valide');
            $table->boolean('statut')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_detail_investiment');
    }
}

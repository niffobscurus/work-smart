<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailInvest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_invest', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user');
            $table->string('libelle');
            $table->text('description')->nullable();
            $table->integer('pourcentage')->default('0');
            $table->integer('temps')->default('0');
            $table->dateTime('date_begin_invest')->nullable();
            $table->dateTime('date_end_invest')->nullable();
            $table->integer('deadline_min_withdrawal')->default('0');
            $table->integer('deadline_max_withdrawal')->default('0');
            $table->integer('min_invest')->default('0');
            $table->integer('max_invest')->default('0');
            $table->integer('deadline_capital_min_withdrawal')->default('0');
            $table->integer('deadline_capital_max_withdrawal')->default('0');
            $table->text('advantage')->nullable();
            $table->text('disadvantage')->nullable();
            $table->string('image')->nullable();
            $table->string('doc_one')->nullable();
            $table->string('doc_two')->nullable();
            $table->string('doc_three')->nullable();
            $table->string('doc_four')->nullable();
            $table->string('etat')->default('valide');
            $table->boolean('statut')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_invest');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVefWithdrawalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vef_withdrawals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user',30);
            $table->string('method');
            $table->biginteger('phone_number');
            $table->biginteger('account');
            $table->string('reference')->unique();
            $table->boolean('statut')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vef_withdrawals');
    }
}

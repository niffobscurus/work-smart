<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoricalInvestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historical_invests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('id_invest')->unsigned();
            $table->string('libelle');
            $table->string('method');
            $table->biginteger('account');
            $table->string('ticket');
            $table->string('etat');
            $table->foreign('id_invest')->references('id')->on('invests')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historical_invests');
    }
}

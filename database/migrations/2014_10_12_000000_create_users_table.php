<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',25);
            $table->string('first_name',45);
            $table->string('birthday')->nullable();
            $table->string('email')->unique();
            $table->timestamp('vef_email')->nullable();
            $table->string('country',25);
            $table->string('town',25)->nullable();
            $table->string('address',250)->nullable();
            $table->biginteger('number_phone');
            $table->string('user',15)->unique();
            $table->string('password');
            $table->string('passgener')->nullable();
            $table->string('token')->nullable();
            $table->boolean('isadmin')->default('0');
            $table->biginteger('balance_cfa')->default('0');
            $table->biginteger('balance_dollar')->default('0');
            $table->integer('percent')->default('0');
            $table->biginteger('balance_cfa_min')->default('5000');
            $table->biginteger('balance_dollar_min')->default('10');
            $table->biginteger('balance_cfa_max')->default('5000000');
            $table->biginteger('balance_dollar_max')->default('100000');
            $table->biginteger('maxaccount')->default('5000000');
            $table->string('avatar')->default('avatars/default.jpg');
            $table->boolean('statut')->default('0');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

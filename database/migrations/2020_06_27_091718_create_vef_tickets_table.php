<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVefTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vef_tickets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('id_ticket_gen')->unsigned();
            $table->string('user_ticket',25);
            $table->biginteger('account');
            $table->string('method');
            $table->biginteger('phone_number');
            $table->string('ticket_generate');
            $table->string('another_info')->nullable();
            $table->string('etat');
            $table->boolean('statut')->default('0');
            $table->foreign('id_ticket_gen')->references('id')->on('tickets')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vef_tickets');
    }
}

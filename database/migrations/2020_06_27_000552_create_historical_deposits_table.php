<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoricalDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historical_deposits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('id_deposit')->unsigned();
            $table->string('libelle');
            $table->string('method');
            $table->biginteger('account');
            $table->string('ticket');
            $table->string('etat');
            $table->foreign('id_deposit')->references('id')->on('deposits');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historical_deposits');
    }
}

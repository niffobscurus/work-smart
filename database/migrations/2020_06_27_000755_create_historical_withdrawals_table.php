<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoricalWithdrawalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historical_withdrawals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('id_withdrawal')->unsigned();
            $table->string('libelle');
            $table->string('method');
            $table->biginteger('account');
            $table->string('ticket');
            $table->string('etat');
            $table->foreign('id_withdrawal')->references('id')->on('withdrawals')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historical_withdrawals');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('min_deposit_cfa');
            $table->bigInteger('maw_deposit_cfa');
            $table->biginteger('min_deposit_dollar');
            $table->bigInteger('max_deposit_dollar');
            $table->bigInteger('min_withdrawal_cfa');
            $table->bigInteger('max_withdrawal_cfa');
            $table->bigInteger('min_withdrawal_dollar');
            $table->bigInteger('max_withdrawal_dollar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}

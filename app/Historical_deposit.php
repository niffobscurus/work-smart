<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historical_deposit extends Model
{
    protected $fillable = ['id', 'id_deposit','libelle','method','account','ticket','etat','created_at'];

    public $primaryKey = 'id';

    //Setter


    // Hidden Proprity

   protected $hidden = [];
}

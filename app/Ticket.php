<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = ['id','user','method','phone_number','account','ticket_generate', 'another_info','etat','statut','created_at'];

    public $primaryKey = 'id';

    //Setter
    public function setUserAttribute($value){
    	$value = auth()->user()->user; 
    	return $this->attributes['user'] = $value;
    }

    // Hidden Proprity

   protected $hidden = [];
}

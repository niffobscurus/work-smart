<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\SystemHelper as syst;

class Invest extends Model
{
    protected $fillable = ['id', 'id_user','account', 'user','ticket', 'etat','number_invest','statut','date_end','created_at'];

    public $primaryKey = 'id';

    //Setter

    public function setIdUserAttribute($value){
    	$value = auth()->user()->id; 
    	return $this->attributes['id_user'] = $value;
    }

    public function setUserAttribute($value){
    	$value = auth()->user()->user; 
    	return $this->attributes['user'] = $value;
    }

    //modification et attribution d'un ticket
    public function setTicketAttribute($value){
    	$value = syst::NTicket(auth()->user()->id);
    	return $this->attributes['ticket'] = $value;
    }

    //modification et attribution "Etat"
    public function setEtatAttribute($value){
    	$value = 'Valide';
    	return $this->attributes['etat'] = $value;
    }


    // Hidden Proprity

   protected $hidden = ['statut'];
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Information_systeme extends Model
{
    protected $fillable = ['id', 'nombre_pers','nombre_invest','nombre_deposit','nombre_withdrawal', 'created_at'];

    public $primaryKey = 'id';

    //Setter


    // Hidden Proprity

   protected $hidden = ['id'];
}
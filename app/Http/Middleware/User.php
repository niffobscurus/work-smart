<?php

namespace App\Http\Middleware;

use Closure;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->guest()){

            return redirect('connexion')->with('message','Vous devez vous connecter pour avoir accès aux informations.');
        }

        if(auth()->user()->isadmin == 1 or auth()->user()->isadmin == 2){
            return back();
        }

        return $next($request);
    }
}

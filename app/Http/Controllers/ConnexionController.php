<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ConnexionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("Connexion");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $messages=[
            'required' => 'Ce champ est requis.'
        ];

        $Validator=Validator::make(
            $request->all(),
            [
                'user'             => 'required|min:4',
                'password'          => 'required|min:4',
            ],
            $messages

        );

        if($Validator->fails()){

            //dd($Validator->messages());
            $Response = $Validator->messages();

        }else{
             //$Response =['success' => 'ok ok '];

            //Controlle des donnees dans la base de donnes
                        $resultat = auth()->attempt([
                            'user' => request('user'),
                            'password' => request('password'),
                            'isadmin' => 0 ,
                        ]);

            // Controlle des donnée d un admin
                        $resultat3 = auth()->attempt([
                            'user' => request('user'),
                            'password' => request('password'),
                            'isadmin' => 1 ,
                        ]);

            // Controlle des donnée d un admin
                        $resultat4 = auth()->attempt([
                            'user' => request('user'),
                            'password' => request('password'),
                            'isadmin' => 2 ,
                        ]);

            // Email pas encore validé
                    $resultat1 = auth()->attempt([
                            'user' => request('user'),
                            'password' => request('password'),
                            'vef_email' => null,
                        ]);


                    if($resultat1){
                        $Response =['success' => 'Pour activer votre compte, veuillez valider votre email'];
                        $Response =['success' => 'con_user_email'];

                        //return back()->with('error','Pour activer votre compte, veuillez valider votre email.');
                    }

            //
                if(!$resultat1){
                    if($resultat){
                        $Response =['success' => 'Profitez de votre Espace'];
                        $Response =['success' => 'con_user'];

                        //return redirect('homeuser')->with('message','Profitez de votre Espace.');
                    }else{
                        $Response =['success' => 'Impo_U'];
                    }

                }

            // Vérifiaction si c est un admin qui c'est connecté
                if(!$resultat){

                    if($resultat3){
                        $Response =['success' => 'Profitez de votre Espace'];
                        $Response =['success' => 'con_admin'];

                        //return redirect('homeadmin')->with('message','Profitez de votre Espace.');
                    }else{
                        // Vérifiaction si c est un  Super Admin qui c'est connecté
                        if($resultat4){
                            $Response =['success' => 'Profitez de votre Espace'];
                            $Response =['success' => 'con_super_admin'];
                        }else
                            // Vérifiaction si c est un admin qui c'est connecté
                            {
                            $Response =['success' => 'Impo_AD'];
                        }
                    }


                    
                }
        }


        //return response()->json(["ok"],200);
        return response()->json($Response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

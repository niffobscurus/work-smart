<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DB;
use App\Deposit;
use App\Setting;
use App\Helpers\DbHelper as helps;
use App\Add\SenderSms;

class DepositeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("User/DepositeUser");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $value_cfa = Setting::find(1);
            $messages=[
                'required' => 'Ce champ est requis.'
            ];

                $Validator=Validator::make(
                    $request->all(),
                    [
                        'account'           => 'required|integer|min:'.$value_cfa->min_deposit_cfa.'|max:'.$value_cfa->maw_deposit_cfa,
                        'phone_number'      => 'required|integer|min:7|max:999999999999999999',
                        'reference'         => 'required|min:1|max:28',
                        'verification'      => 'required',
                        'method'            => 'required',
                    ],
                    $messages

        );

        if($Validator->fails()){
                $Response = $Validator->messages();

        }else{
            // Double Information deposit
            $vef_exit_deposit = Deposit::select(['id'])->whereAccount($request->account)->whereReference($request->reference)
            ->wherePhoneNumber($request->phone_number)->whereMethod($request->method)->first();

            //Reference Check result

            $vef_reference_exit_deposit = Deposit::select(['id'])->whereReference($request->reference)->whereStatut('1')->get();
            //dd($vef_reference_exit_deposit);

            if($vef_exit_deposit){

                 $Response =['success' => 'val_exit'];
            }else{
                if($vef_reference_exit_deposit){
                    //ref
                    $send = new SenderSms();
                    $send = $send->Submit("Votre dépôt a été validé","22893941653");

                    //response
                    $Response =['success' => 'val_ref_exit'];
                }
                    else{
                         $Response =['success' => 'val_ins'];

                         //Insertion des donnees dans la base de donnes
                         $request['id_user'] = "";
                         $request['user'] = "";
                         $request['ticket'] = "";
                         $request['etat'] = "non valide";

                         $deposit=Deposit::create($request->all());
                         helps::AddDeposit();
                         //Mail::to($user)->send(new RegisterMail($user));                
                    }
            
            }

        }

            return response()->json($Response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id =  represente la methode a enregistre
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $method = $id;

        // Title of Methode Explain
        $title_methode = "Deposits Tmoney";
        return view("User/DetailDepositUser",compact("title_methode","method"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DB;
use App\Withdrawal;
use App\Setting;
use App\User;
use App\Helpers\DbHelper as helps;

class WithdrawalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("User.WithdrawalUser");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $value_cfa = Setting::find(1);
            $messages=[
                'required' => 'Ce champ est requis.'
            ];

                $Validator=Validator::make(
                    $request->all(),
                    [
                        'account'           => 'required|integer|min:'.$value_cfa->min_withdrawal_cfa.'|max:'.$value_cfa->max_withdrawal_cfa,
                        'phone_number'      => 'required|integer|min:7|max:999999999999999999',
                        'verification'      => 'required',
                        'method'            => 'required',
                    ],
                    $messages

        );

        if($Validator->fails()){
                $Response = $Validator->messages();

        }else{

                 //Insertion des donnees dans la base de donnes
                 $request['id_user'] = "";
                 $request['user'] = "";
                 $request['ticket'] = "";
                 $request['etat'] = "non valide";

                 $user = User::select(['balance_cfa'])->whereUser(auth()->user()->user)->get();
                 // Verification if user exit
                 if($user){
                    if(auth()->user()->balance_cfa+0 > $request->account+0){
                         $deposit=Withdrawal::create($request->all());
                         //Number of withdrawal
                         helps::AddWithdrawal();

                         //Mise a jour du nouveau valeur restant dans la BD
                        auth()->user()->update([
                            'balance_cfa' => (auth()->user()->balance_cfa - $request->account),
                        ]);

                        // ***********Envoie de mail -------------

                        $Response =['success' => 'val_ins'];
                    }else{

                        $Response =['success' => 'error_ins'];
                    }
                 }
                 //Mail::to($user)->send(new RegisterMail($user));                

        }

            return response()->json($Response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $method = $id;
        return view("User/DetailWithdrawalUser",compact("method"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

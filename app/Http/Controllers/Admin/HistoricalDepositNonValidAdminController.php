<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use App\Helpers\HtmlHelper as html;
use App\Deposit;

class HistoricalDepositNonValidAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("Admin.historicalDepositNonValidAdmin");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataDeposit = Deposit::find($id);
        return $dataDeposit;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view("Admin.HistoricalDepositNonValidAdmin");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * List of Historical Depsoites Not Valid
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function historicalDeposite(){

        $dataDepot = Deposit::select(['id','phone_number','user','account','reference','statut','etat','created_at'])->whereStatut('0')->orderByDesc('id')->get();
        //dd($dataDepot);

        return DataTables::of($dataDepot)
            ->rawColumns(['action','etat_color'])
            ->editColumn('action', function($model){
                $view = html::viewBt($model->id);
                return ' '. $view;
            })
            ->editColumn('etat_color', function(Deposit $dataDepot){
                if ($dataDepot->statut == 0) {
                    $view = html::viewPoint("orange");
                    return ' '. $view;
                }else{
                    if ($dataDepot->statut == 1) {
                        $view = html::viewPoint("green");
                        return ' '. $view;
                    }else{
                        if ($dataDepot->statut == 2) {
                        $view = html::viewPoint("red");
                        return ' '. $view;
                        }
                    }
                }
            })
            ->editColumn('phone_number', function(Deposit $dataDepot) {
                    return $dataDepot->phone_number."";
            })->setRowAttr(['align' => 'center'])
        ->make(true);

    }
}

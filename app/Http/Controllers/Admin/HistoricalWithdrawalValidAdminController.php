<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use App\Helpers\HtmlHelper as html;
use App\Withdrawal;

class HistoricalWithdrawalValidAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("Admin.HistoricalWithdrawalValidAdmin");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataWithdrawal = Withdrawal::find($id);
        return $dataWithdrawal;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * List of Historical Withdrawal
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function historicalWithdrawal(){

        $dataWithdrawal = Withdrawal::select(['id','phone_number','user','account','ticket','statut','etat','created_at'])->whereStatut('1')->orderByDesc('id')->get();

        return DataTables::of($dataWithdrawal)
            ->rawColumns(['action','etat_color'])
            ->editColumn('action', function($model){
                $view = html::viewBt($model->id);
                return ' '. $view;
            })
            ->editColumn('etat_color', function(Withdrawal $dataWithdrawal){
                if ($dataWithdrawal->statut == 0) {
                    $view = html::viewPoint("orange");
                    return ' '. $view;
                }else{
                    if ($dataWithdrawal->statut == 1) {
                        $view = html::viewPoint("green");
                        return ' '. $view;
                    }else{
                        if ($dataWithdrawal->statut == 2) {
                        $view = html::viewPoint("red");
                        return ' '. $view;
                        }
                    }
                }
            })
            ->editColumn('phone_number', function(Withdrawal $dataWithdrawal) {
                    return $dataWithdrawal->phone_number."";
            })->setRowAttr(['align' => 'center'])
        ->make(true);

    }
}

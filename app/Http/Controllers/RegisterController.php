<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
//use App\Mail\RegisterMail;
use App\Helpers\DbHelper as helps;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("Register");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $messages=[
                'required' => 'Ce champ est requis.'
            ];

                $Validator=Validator::make(
                    $request->all(),
                    [
                        'name'               => 'required|min:2|max:20',
                        'first_name'            => 'required|min:2|max:25',
                        'email'             => 'required|unique:users|email',
                        'country'              => 'required',
                        'number_phone'            => 'required|integer|min:9999',
                        'user'             => 'required|min:3|max:12|unique:users,user',
                        'password'          => 'required|confirmed|min:4|max:12',
                        'password_confirmation' => 'required|min:4|max:12',
                    ],
                    $messages

        );

        if($Validator->fails()){
                $Response = $Validator->messages();

        }else{
                 $Response =['success' => 'val_ins'];

                 //Insertion des donnees dans la base de donnes
                 $request['token'] =Str::random(60);

                 $user=User::create($request->all());
                 //Mail::to($user)->send(new RegisterMail($user));
                 helps::AddUser();


        }

            return response()->json($Response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

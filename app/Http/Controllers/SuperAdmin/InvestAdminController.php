<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Helpers\HtmlHelper as html;
use DataTables;
use DB;
use App\Detail_invest;
use App\Type_detail_investiment;
use App\Setting;

class InvestAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_type_invests = Type_detail_investiment::all();
        return view("SuperAdmin.InvestSAdmin",compact("data_type_invests"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Historical LIst for Invest (Pack)
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function historicalInvest(){
        $dataHistorical = Type_detail_investiment::select(['id', 'user','libelle','description','value_one','statut','etat','created_at'])->orderByDesc('id')->get();
        //dd($dataHistorical);

        return DataTables::of($dataHistorical)
            ->rawColumns(['action','etat_color'])
            ->editColumn('action', function($model){
                $view = html::viewBt($model->id);
                return ' '. $view;
            })
            ->editColumn('etat_color', function(Type_detail_investiment $dataHistorical){
                if ($dataHistorical->statut+0 == 0) {
                    $view = html::viewPoint("orange");
                    return ' '. $view;
                }else{
                    if ($dataHistorical->statut+0 == 1) {
                        $view = html::viewPoint("green");
                        return ' '. $view;
                    }else{
                        if ($dataHistorical->statut+0 == 2) {
                        $view = html::viewPoint("red");
                        return ' '. $view;
                        }
                    }
                }
            })
            ->editColumn('libelle', function(Type_detail_investiment $dataHistorical) {
                    return $dataHistorical->libelle."";
            })->setRowAttr(['align' => 'center'])
        ->make(true);

    }
}

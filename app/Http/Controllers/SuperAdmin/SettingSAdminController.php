<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use DB;
use App\Setting;

class SettingSAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $value_cfa = Setting::find(1);
        return view("SuperAdmin.SettingSAdmin",compact("value_cfa"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /* Function de la mise a jour des champs price */    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update_cfa(Request $request)
    {

        $value_cfa = Setting::find(1);
            $messages=[
                'required' => 'Ce champ est requis.'
            ];

                $Validator=Validator::make(
                    $request->all(),
                    [
                        'min_deposit_cfa'           => 'integer|min:100|max:99999999',
                        'maw_deposit_cfa'      => 'integer|min:'.$value_cfa->min_deposit_cfa.'|max:999999999999999999',
                    ],

                    $messages

        );

        if($Validator->fails()){
                $Response = $Validator->messages();

        }else{
                 $Response =['success' => 'val_ins'];

                 //Insertion des donnees dans la base de donnes des mise a jour 
                 $value_cfa = Setting::find(1);

                 $value_cfa->update($request->all());

                 /**** Date de la mise a jour et qui l"a fait */


        }

            return response()->json($Response,200);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update_dollar(Request $request)
    {
        // Selection information for

        $value_cfa = Setting::find(1);
            $messages=[
                'required' => 'Ce champ est requis.'
            ];

                $Validator=Validator::make(
                    $request->all(),
                    [
                        'min_deposit_dollar'           => 'integer|min:100|max:99999999999',
                        'max_deposit_dollar'      => 'integer|min:'.$value_cfa->min_deposit_dollar.'|max:9999999999999',
                    ],

                    $messages

            );

        if($Validator->fails()){
                $Response = $Validator->messages();

        }else{
                 $Response =['success' => 'val_ins'];

                 //Insertion des donnees dans la base de donnes des mise a jour 

                 $value_cfa->update($request->all());

                 /**** Date de la mise a jour et qui l"a fait */


        }

            return response()->json($Response,200);
    }

    /* Function de la mise a jour des champs price des retrait */    

    /**
     * Withdrawal limite CFA.
     *
     * @return \Illuminate\Http\Response
     */
    public function update_withdrawal_cfa(Request $request)
    {

        $value_cfa = Setting::find(1);
            $messages=[
                'required' => 'Ce champ est requis.'
            ];

                $Validator=Validator::make(
                    $request->all(),
                    [
                        'min_withdrawal_cfa'           => 'integer|min:100|max:99999999',
                        'max_withdrawal_cfa'      => 'integer|min:'.$value_cfa->min_withdrawal_cfa.'|max:999999999999999999',
                    ],

                    $messages

        );

        if($Validator->fails()){
                $Response = $Validator->messages();

        }else{
                 $Response =['success' => 'val_ins'];

                 //Insertion des donnees dans la base de donnes des mise a jour 
                 $value_cfa = Setting::find(1);

                 $value_cfa->update($request->all());

                 /**** Date de la mise a jour et qui l"a fait */


        }

            return response()->json($Response,200);
    }


    /**
     * Withdrawal limite  Dollar.
     *
     * @return \Illuminate\Http\Response
     */
    public function update_withdrawal_dollar(Request $request)
    {
        // Selection information for

        $value_cfa = Setting::find(1);
            $messages=[
                'required' => 'Ce champ est requis.'
            ];

                $Validator=Validator::make(
                    $request->all(),
                    [
                        'min_withdrawal_dollar'           => 'integer|min:100|max:99999999999',
                        'max_withdrawal_dollar'      => 'integer|min:'.$value_cfa->min_withdrawal_dollar.'|max:9999999999999',
                    ],

                    $messages

            );

        if($Validator->fails()){
                $Response = $Validator->messages();

        }else{
                 $Response =['success' => 'val_ins'];

                 //Insertion des donnees dans la base de donnes des mise a jour 

                 $value_cfa->update($request->all());

                 /**** Date de la mise a jour et qui l"a fait */


        }

            return response()->json($Response,200);
    }

}

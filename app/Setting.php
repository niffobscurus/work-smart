<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['id','min_deposit_cfa','maw_deposit_cfa','min_deposit_dollar','max_deposit_dollar','min_withdrawal_cfa','max_withdrawal_cfa','min_withdrawal_dollar','max_withdrawal_dollar','created_at'];

    public $primaryKey = 'id';

    //Setter

    // Hidden Proprity

   protected $hidden = [];
}

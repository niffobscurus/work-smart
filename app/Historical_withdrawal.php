<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historical_withdrawal extends Model
{
    protected $fillable = ['id', 'id_withdrawal','libelle','method','account','ticket','etat','created_at'];

    public $primaryKey = 'id';

    //Setter


    // Hidden Proprity

   protected $hidden = [];
}

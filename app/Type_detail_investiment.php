<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type_detail_investiment extends Model
{
    protected $fillable = ['id','user','libelle','description','value_one', 'etat','statut','created_at'];

    public $primaryKey = 'id';

    //Setter
    public function setUserAttribute($value){
    	$value = auth()->user()->user; 
    	return $this->attributes['user'] = $value;
    }

    // Hidden Proprity

   protected $hidden = [];
}

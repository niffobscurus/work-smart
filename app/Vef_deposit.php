<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vef_deposit extends Model
{
    protected $fillable = ['id','user','method','phone_number','account','reference','statut','created_at'];

    public $primaryKey = 'id';

    //Setter
    public function setUserAttribute($value){
    	$value = auth()->user()->user; 
    	return $this->attributes['user'] = $value;
    }


    // Hidden Proprity

   protected $hidden = [];
}

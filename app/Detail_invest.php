<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_invest extends Model
{
    protected $fillable = ['id','user','libelle','description','pourcentage', 'temps', 'date_begin_invest', 'date_end_invest','deadline_min_withdrawal','deadline_max_withdrawal', 'min_invest', 'max_invest', 'deadline_capital_min_withdrawal', 'deadline_capital_max_withdrawal', 'advantage','disadvantage','image', 'min_invest', 'doc_one', 'doc_two', 'doc_three', 'doc_four', 'etat', 'statut', 'created_at', 'update_at'];

    public $primaryKey = 'id';

    //Setter
    public function setUserAttribute($value){
    	$value = auth()->user()->user; 
    	return $this->attributes['user'] = $value;
    }

    // Hidden Proprity

   protected $hidden = [];
}

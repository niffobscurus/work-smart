<?php

namespace App\Helpers;

class HtmlHelper{

	public static function editBt($url){
		return '<a href="'.$url.'" class="btn btn-outline-secondary"><i class="fa fa-edit"></i></a>';
	}

	public static function deleteBt($url){
		return '<a onclick="showData('.$url.')" class="btn btn-outline-info"><i class="fa fa-eye"></i></a>';
	}

	public static function viewBt($url){
		return '<a onclick="showData('.$url.')" class="btn btn-outline-info"><i class="fa fa-eye"></i></a>';
	}

	public static function viewPoint($color){
		return '<i class="fas fa-circle red-text text-center" style="color:'.$color.'"></i></a>';
	}

}

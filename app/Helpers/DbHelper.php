<?php

namespace  App\Helpers;

use Carbon\Carbon;
use App\Information_systeme;

class DbHelper{
	// function qui compte le nombre de personne inscrit
	public static function AddUser(){
		$valeur = Information_systeme::all()->take(1);
		$valeur_actuel = $valeur[0]->nombre_pers;
		$valeur_ajout = $valeur_actuel+1;

		$valeur[0]->update([
			'nombre_pers' => $valeur_ajout,
		]);
	}

	// function qui compte le nombre de personne ayant effectuer un dons
	public static function AddInvest(){
		$valeur = Information_systeme::all()->take(1);
		$valeur_actuel = $valeur[0]->nombre_invest;
		$valeur_ajout = $valeur_actuel+1;

		$valeur[0]->update([
			'nombre_invest' => $valeur_ajout,
		]);
	}

	// function qui compte le nombre de personne qui ont effectuer un dépôt
	public static function AddDeposit(){
		$valeur = Information_systeme::all()->take(1);
		$valeur_actuel = $valeur[0]->nombre_deposit;
		$valeur_ajout = $valeur_actuel+1;

		$valeur[0]->update([
			'nombre_deposit' => $valeur_ajout,
		]);
	}

	// function qui compte le nombre de personne qui ont effectuer un rétrait
	public static function AddWithdrawal(){
		$valeur = Information_systeme::all()->take(1);
		$valeur_actuel = $valeur[0]->nombre_withdrawal;
		$valeur_ajout = $valeur_actuel+1;

		$valeur[0]->update([
			'nombre_withdrawal' => $valeur_ajout,
		]);
	}
}
<?php

namespace App\Helpers;

use Carbon\Carbon;
use App\Withdrawal;
use App\User;

class SystemHelper{

	//Fonction pour generer un ticket a un utilisateur
	public static function NTicket($id){
		return ('T'.date("YmdHms").$id);
	}


	/*Fonction de validation de véfication du retrait " < au niveau de la page de l'ulisateur> "
	* @retrait : variable de verification de l'existance d'un retrait
	* @utilisateur : variable de décompte de la varaible " Solde "de la tabel "Utlisateurs"
	* Etat : pas utiliser dans le code
	*/
	public static function VFRetrait($request){
        $utilisateur = User::findOrFail(auth()->user()->id);

        if($utilisateur){
        $retrait = Retrait::whereMethode($request->methode)->whereMontant($request->montant)->whereNumero($request->numero)->whereStatut(0)->first();
        	//dd($vefretrait);

        	$retrait->update([
    			'etat' => 'non valide',
    			'statut' => 0,
        	]);

        	$utilisateur->update([
        		'solde' => ($utilisateur->solde - $request->montant),
        	]);
        }

	}


}


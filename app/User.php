<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'first_name', 'birthday', 'email', 'vef_email', 'country', 'town', 'address', 'number_phone', 'user','password', 'passgener', 'token', 'isadmin', 'balance_cfa', 'balance_dollar', 'percent', 'balance_cfa_min', 'balance_dollar_min', 'balance_cfa_max', 'balance_dollar_max', 'maxaccount', 'avatar', 'remember_token', 'created_at'
    ];


    /**
     * Cet mutatteur permet de crypter le mot de passe avant de l'envoie dans la base de données
     *
     * @return string
     */
    public function setPasswordAttribute($value){
         return $this->attributes['password'] = bcrypt($value);
        }


    /**
     * Cette function convertir l'email en miniscule avent d'uploader dans la base de données
     *
     * @return string
     */
    public function setEmailAttribute($value){
        return $this->attributes['email'] = strtolower($value);
    }



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id','password' ,'passgener', 'remember_token','isadmin','maxaccount','vef_email'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}

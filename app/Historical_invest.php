<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historical_invest extends Model
{
    protected $fillable = ['id', 'id_invest','libelle','method','account','ticket','etat','created_at'];

    public $primaryKey = 'id';

    //Setter


    // Hidden Proprity

   protected $hidden = [];
}

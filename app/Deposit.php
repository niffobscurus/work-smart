<?php

namespace App;

use App\Helpers\SystemHelper as syst;
use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    protected $fillable = ['id', 'id_user', 'account', 'user', 'reference', 'phone_number', 'method', 'ticket', 'etat', 'statut', 'created_at'];

    public $primaryKey = 'id';

    //Setter
    public function setIdUserAttribute($value){
    	$value = auth()->user()->id; 
    	return $this->attributes['id_user'] = $value;
    }
    

    //modifier la valeur du nom
    public function setUserAttribute($value){
    	$value = auth()->user()->user; 
    	return $this->attributes['user'] = $value;
    }


    //modification et attribution d'un ticket
    public function setTicketAttribute($value){
    	$value = syst::NTicket(auth()->user()->id);
    	return $this->attributes['ticket'] = $value;
    }


    // Hidden Proprity

   protected $hidden = ['statut'];
}
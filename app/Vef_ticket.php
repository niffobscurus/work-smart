<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vef_ticket extends Model
{
    protected $fillable = ['id', 'id_ticket_gen','user_ticket','method','phone_number','account','ticket_generate','another_info','etat','statut','created_at'];

    public $primaryKey = 'id';

    //Setter
    public function setUserTicketAttribute($value){
    	$value = auth()->user()->user; 
    	return $this->attributes['user_ticket'] = $value;
    }

    // Hidden Proprity

   protected $hidden = [];
}

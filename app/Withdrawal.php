<?php

namespace App;

use App\Helpers\SystemHelper as syst;
use Illuminate\Database\Eloquent\Model;

class Withdrawal extends Model
{
    protected $fillable = ['id', 'id_user','user','method','phone_number','account','ticket','etat','statut','created_at'];

    public $primaryKey = 'id';

    //Setter

        //modifier la valeur de Id de l'identifiant
    public function setIdUserAttribute($value){
    	$value = auth()->user()->id; 
    	return $this->attributes['id_user'] = $value;
    }
    

    //modifier la valeur du user
    public function setUserAttribute($value){
    	$value = auth()->user()->user; 
    	return $this->attributes['user'] = $value;
    }


    //modification et attribution d'un ticket
    public function setTicketAttribute($value){
    	$value = syst::NTicket(auth()->user()->id);
    	return $this->attributes['ticket'] = $value;
    }
    

    // Hidden Proprity

   protected $hidden = ['statut'];
}

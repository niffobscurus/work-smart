<?php

return [
	'home' => 'Busy Brain',

	// Les information possible sur une transaction
	'id' 			=> 'Idenfiant',
	'user' 			=> 'Utilisateur',
	'methode' 		=> 'Moyen',
	'numero' 		=> 'Numéro',
	'montant' 		=> 'Montant',
	'reference' 	=> 'Référence',
	'statut' 		=> 'Statut',
	'ticket'		=> 'Ticket',
	'date_fin'		=> 'Date Fin',
	'etat' 			=> 'Etat',
	'created_at' 	=> 'Date et Heure',

	// Les nom des boutons et action
	'action'  		=> ' Action',

	// les mots possible
	'depot' 		=> 'Dépôt',
	'retrait' 		=>'Retrait',
	'dons'			=> 'Investissement',


	//Les actions ménée sur une transaction
	'create' 		=> 'Ajouter :name',

];

@extends('User.layouts.pagesExtends.formAdminIndex',['title'=>' Balance'])
@section('main')

	{{-- CONTAINER PAGE --}}

            <div class="container-fluid">
                <h3 class="text-dark mb-4"><strong>Balance</strong></h3>
                <div class="row">
                    <div class="col-lg-5 offset-lg-1 rubberBand animated">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="text-center text-primary font-weight-bold m-0"><strong>Investors in FCFA</strong><br></h6>
                            </div>
                        </div>
                        <div class="card mb-5">
                            <div class="card-body text-center shadow"><img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="swing" src="assets/img/dogs/image2.jpeg" width="160" height="160">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col text-right col-md-6">
                                            <p class="text-right float-right col-md-12">Balance :&nbsp;</p>
                                        </div>
                                        <div class="col text-left float-left col-md-6">
                                            <p class="float-left col-md-12">{{ auth()->user()->balance_cfa }} XOF</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3" style="margin-top: 5%;">
                                    <a href="/depositeUser"><button class="btn btn-success btn-sm" data-bs-hover-animate="pulse" type="button">Make deposit</button></a>
                                    <a href="/withdrawalUser"> <button class="btn btn-primary btn-sm offset-lg-1" data-bs-hover-animate="pulse" type="button">Make withdrawal</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="row mb-3 d-none">
                            <div class="col">
                                <div class="card text-white bg-primary shadow">
                                    <div class="card-body">
                                        <div class="row mb-2">
                                            <div class="col">
                                                <p class="m-0">Peformance</p>
                                                <p class="m-0"><strong>65.2%</strong></p>
                                            </div>
                                            <div class="col-auto"><i class="fas fa-rocket fa-2x"></i></div>
                                        </div>
                                        <p class="text-white-50 small m-0"><i class="fas fa-arrow-up"></i>&nbsp;5% since last month</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card text-white bg-success shadow">
                                    <div class="card-body">
                                        <div class="row mb-2">
                                            <div class="col">
                                                <p class="m-0">Peformance</p>
                                                <p class="m-0"><strong>65.2%</strong></p>
                                            </div>
                                            <div class="col-auto"><i class="fas fa-rocket fa-2x"></i></div>
                                        </div>
                                        <p class="text-white-50 small m-0"><i class="fas fa-arrow-up"></i>&nbsp;5% since last month</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 offset-lg-2 rubberBand animated">
                                <div class="card shadow mb-4">
                                    <div class="card-header py-3">
                                        <h6 class="text-center text-primary font-weight-bold m-0"><strong>Investors in Dollar</strong></h6>
                                    </div>
                                </div>
                                <div class="card mb-3">
                                    <div class="card-body text-center shadow"><img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="swing" src="assets/img/dogs/image2.jpeg" width="160" height="160">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col text-right col-md-6">
                                                    <p class="text-right float-right col-md-12">Balance :&nbsp;</p>
                                                </div>
                                                <div class="col text-left float-left col-md-6">
                                                    <p class="float-left col-md-12">{{ auth()->user()->balance_dollar }} $</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3" style="margin-top: 5%;">
                                            <a href="/depositeUser"><button class="btn btn-success btn-sm" data-bs-hover-animate="pulse" type="button">Make deposit</button></a>
                                            <a href="/withdrawalUser"><button class="btn btn-primary btn-sm offset-lg-1" data-bs-hover-animate="pulse" type="button">Make withdrawal</button></div></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

{{-- END CONTAINER PAGE --}}

@stop




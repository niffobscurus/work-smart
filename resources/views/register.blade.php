@extends('layouts.pagesExtends.formAdminIndex',['title'=>' Register'])
@section('main')

	{{-- CONTAINER PAGE --}}



    <div class="container">
        <div class="card shadow-lg o-hidden border-0 my-5">
            <div class="card-body p-0">
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-flex">
                        <div class="flex-grow-1 bg-register-image" style="background-image: url(&quot;assets/img/dogs/image2.jpeg&quot;);"></div>
                    </div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h4 class="text-dark mb-4">Create an Account!</h4>
                            </div>
                            <form class="user" method="" id="register" role="form">
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input class="form-control form-control-user" type="text" id="name" placeholder="First Name" name="name">
                                        <p id="error_name" class="text-center text-danger"></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <input class="form-control form-control-user" type="text" id="first_name" placeholder="Last Name" name="first_name">
                                        <p id="error_first_name" class="text-center text-danger"></p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input class="form-control form-control-user" type="email" id="email" aria-describedby="emailHelp" placeholder="Email Address" name="email">
                                    <p id="error_email" class="text-center text-danger"></p>
                                </div>

                                <div class="form-group">
                                    <input class="form-control form-control-user" type="text" id="user" aria-describedby="emailHelp" placeholder="Pseudo" name="user">
                                    <p id="error_user" class="text-center text-danger"></p>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">

                                        <select class="form-control " id="country"  name="country" placeholder="Pays">
                                        </select>

                                        <p id="error_country" class="text-center text-danger"></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <input class="form-control" type="tel" id="number_phone" placeholder="numero" name="number_phone">
                                        <p id="error_phone_number" class="text-center"></p>
                                        <span id="valid-msg" class="hide"> Valide </span>
                                        <span id="error-msg" class="hide"> </span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input class="form-control form-control-user" type="password" id="password" placeholder="Password" name="password">
                                        <p id="error_password" class="text-center text-danger"></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <input class="form-control form-control-user" type="password" id="password_confirmation" placeholder="Repeat Password" name="password_confirmation">
                                        <p id="error_password_confirmation" class="text-center text-danger"></p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <input class="form-control form-control-user" type="hidden" id="token" name="token">
                                </div>
                                <div>
                                    <p> J'accepte les termes du contract et attest avoir 18 ans.</p>
                                </div>

                                <button class="btn btn-primary btn-block text-white btn-user" id="submit" type="button">Register Account</button>
                                <hr>
                                <a class="btn btn-primary btn-block text-white btn-google btn-user" role="button">
                                    <i class="fab fa-google"></i>&nbsp; Register with Google
                                </a>
                                    <a class="btn btn-primary btn-block text-white btn-facebook btn-user" role="button"><i class="fab fa-facebook-f"></i>&nbsp; Register with Facebook</a>
                                <hr>
                            </form>
                            <div class="text-center"><a class="small" href="forgot-password.html">Forgot Password?</a></div>
                            <div class="text-center"><a class="small" href="/connexion">Already have an account? Login!</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


{{-- Js Aditionnal --}}
<script src="{{asset('build/js/intlTelInput.min.js')}}"></script>

{{-- End Js Aditionnal --}}


                    <script>
                        $(document).ready(function () {

                            $('#submit').click(function () {

                                // Accord de confidentialiter
                                Swal.fire({
                                  title: 'Terme et accord',
                                  text: "J'accepte les termes du contract et attest avoir 18 ans.",
                                  icon: 'question',
                                  showCancelButton: true,
                                  confirmButtonColor: '#3085d6',
                                  cancelButtonColor: '#d33',
                                  confirmButtonText: "Oui, je suis d'accord"
                                }).then((result) => {
                                  if (result.value) {
                                    Swal.fire(
                                      'Tanks',
                                      'Merci pour votre accord.',
                                      'success'
                                    )


                                var data = $('#register').serialize();
                                // alert(data);
                                $.ajax({
                                    type: 'post',
                                    data: data,
                                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                                    url: "{{ route('register.store')}}",
                                    success: function (Response) {
                                        console.log(Response);
                                        //alert("success");

                                        /* Initialisation des champs */
                                        document.getElementById("error_name").innerHTML = ' ';
                                        document.getElementById("error_first_name").innerHTML = ' ';
                                        document.getElementById("error_email").innerHTML = ' ';
                                        document.getElementById("error_user").innerHTML = ' ';
                                        document.getElementById("error_country").innerHTML = ' ';
                                        document.getElementById("error_phone_number").innerHTML = ' ';
                                        document.getElementById("error_password").innerHTML = ' ';
                                        document.getElementById("error_password_confirmation").innerHTML = ' ';


                                        /*validate des champs*/
                                        if (Response.name) {
                                            document.getElementById("error_name").innerHTML = '<p>' + Response.name + '</p>'
                                        }


                                        if (Response.first_name) {
                                            document.getElementById("error_first_name").innerHTML = '<p>' + Response.first_name + '</p>'
                                        }

                                        if (Response.email) {
                                            document.getElementById("error_email").innerHTML = '<p>' + Response.email + '</p>'
                                        }

                                        if (Response.pseudo) {
                                            document.getElementById("error_user").innerHTML = '<p>' + Response.user + '</p>'
                                        }

                                        if (Response.number_phone) {
                                            document.getElementById("error_country").innerHTML = '<p>' + Response.country + '</p>'
                                        }

                                        if (Response.country) {
                                            document.getElementById("error_phone_number").innerHTML = '<p>' + Response.number_phone + '</p>'
                                        }

                                        if (Response.password) {
                                            document.getElementById("error_password").innerHTML = '<p>' + Response.password + '</p>'
                                        }

                                        if (Response.password_confirmation) {
                                            document.getElementById("error_password_confirmation").innerHTML = '<p>' + Response.password_confirmation + '</p>'
                                        }

                                        if(Response.success == "val_ins"){
                                            Swal.fire(
                                              'Votre compte est disponible',
                                              'Veuillez valider votre compte dans votre mail.',
                                              'success'
                                            )
                                            window.location.href = '/connexion';
                                        }



                                    },

                                })
                                  }
                                })


                            });

                        })
                    </script>


<script>
    var input = document.querySelector('#number_phone');
    errorMsg = document.querySelector("#error-msg"),
    validMsg = document.querySelector("#valid-msg");
    var countryData = window.intlTelInputGlobals.getCountryData(),
    addressDropdown = document.querySelector("#country");

    // here, the index maps to the error code returned from getValidationError - see readme
    var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];


    var iti = window.intlTelInput(input,{
        //* iNITIALISATION DU PAYS EN COURS
        //initialCountry :'tg',
        //nationalMode : true,
        //* Hidden du numero d écriture
        //hiddenInput :'numero',

        /* Vérification si le numero est valide
         */

        utilsScript : '{{asset("build/js/utils.js")}}'
    });

    /* */

    // populate the country dropdown
    for (var i = 0; i < countryData.length; i++) {
        var country = countryData[i];
        var optionNode = document.createElement("option");
        optionNode.value = country.iso2;
        var textNode = document.createTextNode(country.name);
        optionNode.appendChild(textNode);
        addressDropdown.appendChild(optionNode);
    }
    // set it's initial value
    addressDropdown.value = iti.getSelectedCountryData().iso2;

    // listen to the telephone input for changes
    input.addEventListener('countrychange', function(e) {
        addressDropdown.value = iti.getSelectedCountryData().iso2;
    });

    // listen to the address dropdown for changes
    addressDropdown.addEventListener('change', function() {
        iti.setCountry(this.value);
    });

    /* Fin du syschronisasion du champs de pays*/

    /* Validation du numéro entré */

    var reset = function() {
        input.classList.remove("error");
        errorMsg.innerHTML = "";
        errorMsg.classList.add("hide");
        validMsg.classList.add("hide");
    };

    // on blur: validate
    input.addEventListener('blur', function() {
        reset();
        if (input.value.trim()) {
            if (iti.isValidNumber()) {
                validMsg.classList.remove("hide");
                $("#number_phone").val(iti.getNumber(intlTelInputUtils.numberFormat.E164));
                //alert();
            } else {
                input.classList.add("error");
                var errorCode = iti.getValidationError();
                errorMsg.innerHTML = errorMap[errorCode];
                errorMsg.classList.remove("hide");
            }
        }
    });

    // on keyup / change flag: reset
    input.addEventListener('change', reset);
    input.addEventListener('keyup', reset);


</script>




{{-- END CONTAINER PAGE --}}

@stop




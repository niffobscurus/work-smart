<script src="{{asset('assets/js/datatable/jquery.dataTables.min.js')}}"></script>

<script type="text/javascript">

    $(function() {
        // Function pour le chargement des données dans la base de données
        $('#historicalDataTable').DataTable({
            processing: true,
            serverSide: true,
            destroy :true ,
            clear : true,
            paging: true,
            lengthChange : true,
            searching : true,
            ordering : true,
            autoWidth : true,
            ajax: '{{ route('admin.historical_deposit.data') }}',
            columns: [
                { data: 'etat_color' , searchable : false},
                { data: 'id', name: 'id' },
                { data: 'phone_number', name: 'phone_number' },
                { data: 'user', name: 'user' },
                { data: 'account', name: 'account' },
                { data: 'reference', name: 'reference' },
                { data: 'etat', name: 'etat' },
                { data: 'created_at', name: 'created_at' },
                { data: 'action' , searchable : false}
            ]
        });

    });


    // Fonction pour l'affichage du show
    function showData(id) {
        $.ajax({
            url : "{{url('historicalDepositAdmin')}}"+ '/'+ id,
            type : "GET",
            dataType : "JSON",
            success : function (data) {
                $('#modal-info').modal('show');
                $('.modal-title').text("Détails sur le dépôt de "+ data.user);
                $('#info_id').text(data.id);
                $('#info_date_val').text(data.created_at);
                $('#info_name').text(data.user);
                $('#info_montant').text(data.account);
                $('#info_reference').text(data.reference);
                $('#info_etat').text(data.etat);

                //Condiction avancys
                
                //AU
                if(data.method == "AU"){
                    $('#info_methode').text("Autre Methode");
                    $("#image_methode").html('<img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="tada" src="{{asset('assets/img/orther.jpg')}}" width="160" height="160">');
                }
                else{
                    $('#info_methode').text(data.method);
                    $("#image_methode").html('<img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="tada" src="{{asset('assets/img/orther.jpg')}}" width="160" height="160">');
                }

                //Tmoney
                if(data.method == "TY"){
                    $('#info_methode').text("TMONEY");
                    $("#image_methode").html('<img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="tada" src="{{asset('assets/img/tmoney.jpg')}}" width="160" height="160">');
                }

                //Flooz
                if(data.method == "FZ"){
                    $('#info_methode').text("FLOOZ");
                    $("#image_methode").html('<img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="tada" src="{{asset('assets/img/flooz.jpg')}}" width="160" height="160">');
                }
      
                //PM          
                if(data.method == "PM"){
                    $('#info_methode').text("Perfect Money");
                    $("#image_methode").html('<img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="tada" src="{{asset('assets/img/perfectmoney.jpg')}}" width="160" height="160">');
                }
                
                //TICKET
                if(data.method == "CD"){
                    $('#info_methode').text("Code Généré");
                    $("#image_methode").html('<img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="tada" src="{{asset('assets/img/codegeneration.jpg')}}" width="160" height="160">');
                }

                


            },
            error : function () {
                Swal.fire(
                  'Oops',
                  'Veuillez réactualiser la page ou connectez-vous',
                  'warning'
                )
            }
        });
    }

</script>
@extends('Admin.layouts.pagesExtends.formAdminTable',['title'=>' Historical Not Validate Deposit'])
@section('main')

	{{-- CONTAINER PAGE --}}


            <div class="container-fluid jello animated">
                <h3 class="text-dark mb-4">Team</h3>
                <div class="card shadow">
                    <div class="card-header pulse animated py-3">
                        <p class="text-primary m-0 font-weight-bold">Employee Info</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                            <table class="table dataTable my-0" id="historicalDataTable">
                                <thead>
                                    <tr class="text-center">
                                        <th id="etat_color" data-field="id">Statut</th>
                                        <th id="id" data-field="id">Id</th>
                                        <th id="phone_number" data-field="phone_number">Phone Number</th>
                                        <th id="user" data-field="user">User</th>
                                        <th id="account" data-field="account">Account</th>
                                        <th id="reference" data-field="reference">Reference</th>
                                        <th id="etat" data-field="etat">Etat</th>
                                        <th id="created_at" data-field="created_at">Date Création</th>
                                        <th id="action" data-field="action">Action</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                </div>
            </div>


        <div class="modal fade" id="modal-info" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

            <div class="modal-dialog" role="document">

                <div class="modal-content text-center">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">

                        <div class="item form-group">
                            <p id="image_methode"></p>
                        </div>
                        <div class="item form-group">
                            <p class="text-center" style="font-size: 15px;"> Vérification de validation pour le numero :
                                <span id="info_id" style="color: black;"> 000 </span>
                            </p>
                        </div>
                        <br>

                        <div class="item form-group">
                            <p class="text-center" style="font-size: 15px;"> Date de validation :
                                <span id="info_date_val" style="color: black;"> 2019-12-16 </span>
                            </p>
                        </div>
                        <br>

                        <div class="item form-group">
                            <p class="text-center" style="font-size: 15px;"> Dépôt fait par :
                                <span id="info_name" style="color: black;"> Systeme</span>
                            </p>
                        </div>
                        <br>

                        <div class="item form-group">
                            <p class="text-center" style="font-size: 15px;"> Moyen de payement :
                                <span id="info_methode" style="color: black;"> Tmoney </span>
                            </p>
                        </div>
                        <br>

                        <div class="item form-group">
                            <p class="text-center" style="font-size: 15px;"> Montant :
                                <span id="info_montant" style="color: black;"> 000 </span>
                            </p>
                        </div>
                        <br>

                        <div class="item form-group">
                            <p  class="text-center" style="font-size: 15px;"> Réference :
                                <span id="info_reference" style="color: black;"> 000 </span>
                            </p>
                        </div>
                        <br>

                        <div class="item form-group">
                            <p  class="text-center" style="font-size: 15px;"> Etat :
                                <span id="info_etat" style="color: black;">  </span>
                            </p>
                        </div>
                        <br>

                    </div>
                    <div class="modal-footer text-center">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">D'accord</button>
                    </div>
                </div>
            </div>
        </div>



            {{-- FOOTER --}}
            @include('Admin.layouts.partials._js_historical_nonvalid_deposit')
            {{-- END FOOTER --}}

{{-- END CONTAINER PAGE --}}

@stop




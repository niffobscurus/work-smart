@extends('SuperAdmin.layouts.pagesExtends.formAdminIndexTable',['title'=>' Type Investiment PACK'])
@section('main')

	{{-- CONTAINER PAGE --}}


            <div class="container-fluid">
                <h3 class="text-dark mb-4">Type Pack Investiment</h3>
                <div class="row mb-3">
                    <div class="col-lg-12">

                        {{-- Deposite Configuration --}}
                        <div class="row pulse animated">
                            <div class="col pulse animated">
                                <div class="card shadow mb-3" data-aos="flip-left" data-aos-duration="750">
                                    <div class="card-header py-3">
                                        <p class="text-primary m-0 font-weight-bold">Add Package</p>
                                    </div>

                                    {{-- Begin Add Type Pakage --}}

                                    <div class="card-body">
                                        <form id="submit_new_type_invest" method="post">

                                            <div class="form-row d-lg-flex justify-content-lg-center">
                                                <div class="col-lg-10">
                                                    <div class="form-group">
                                                        <label class="d-lg-flex justify-content-lg-start" for="id_residence">
                                                            {{__('choose the residence')}} &nbsp;<span style="color: red;">*</span> 

                                                        </label>

                                                    <select id="id_residence" name="id_residence" autocomplete="off" class="form-control" wire:change="id_residence_valid($event.target.value) " wire:model="id_residence" >
                                                            <option value="">--{{__('choose the residence')}}--</option>
                                                        @foreach($data_type_invests as $data_type_invest)
                                                        
                                                            <option value="{{$data_type_invest->id}}" >
                                                                &nbsp; {{$data_type_invest->libelle}}</option>
                                                        @endforeach
                                                    </select>
                                                    </div> 

                                                </div>
                                            </div>


                                            <div class="form-row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="first_name"><strong>Name of package</strong>&nbsp;<span style="color: red;">*</span></label>
                                                        <input class="form-control" type="text" placeholder="E.g.: Pack Name" name="libelle" id="libelle">
                                                        <p id="error_libelle" class="text-center text-danger"></p>
                                                    </div>
                                                </div>

                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="first_name"><strong>Package Description</strong> &nbsp;<span style="color: red;">*</span> </label>
                                                        <textarea class="form-control" rows="3" placeholder="E.g.: Tranging is pack of Bourse" name="description" id="description"></textarea>
                                                        <p id="error_description" class="text-center text-danger"></p>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="form-row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="first_name"><strong>Package duration</strong>&nbsp;<span style="color: red;">*</span></label>
                                                        <input class="form-control" type="number" placeholder="E.g.: 40" name="libelle" id="libelle">
                                                        <p id="error_libelle" class="text-center text-danger" required=""></p>
                                                    </div>
                                                </div>

                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="first_name"><strong>Package Pourcentage</strong> &nbsp;<span style="color: red;">*</span> </label>
                                                        <input class="form-control" type="number" placeholder="E.g.: 10" name="description" id="description">
                                                        <p id="error_description" class="text-center text-danger"></p>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="form-row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="first_name"><strong>Date Begin Investiment</strong>&nbsp;<span style="color: red;">*</span></label>
                                                        <input class="form-control" type="date" placeholder="E.g.: 02-09-2020" name="libelle" id="libelle">
                                                        <p id="error_libelle" class="text-center text-danger" required=""></p>
                                                    </div>
                                                </div>

                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="first_name"><strong>Date End Investiment</strong> &nbsp;<span style="color: red;">*</span> </label>
                                                        <input class="form-control" type="date" placeholder="E.g.: 02-09-2020" name="description" id="description">
                                                        <p id="error_description" class="text-center text-danger"></p>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="form-row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="first_name"><strong>Number minimal of days for withdrawal </strong>&nbsp;<span style="color: red;">*</span></label>
                                                        <input class="form-control" type="number" placeholder="E.g.: 2 days" name="libelle" id="libelle">
                                                        <p id="error_libelle" class="text-center text-danger" required=""></p>
                                                    </div>
                                                </div>

                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="first_name"><strong>number maximum of days for withdrawal</strong> &nbsp;<span style="color: red;">*</span> </label>
                                                        <input class="form-control" type="number" placeholder="E.g.: 15 days" name="description" id="description">
                                                        <p id="error_description" class="text-center text-danger"></p>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="form-row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="first_name"><strong>Advantage</strong>&nbsp;<span style="color: red;">*</span></label>
                                                        <textarea class="form-control" rows="4" placeholder="E.g.: Description advantage" name="libelle" id="libelle"> </textarea>
                                                        <p id="error_libelle" class="text-center text-danger"></p>
                                                    </div>
                                                </div>

                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="first_name"><strong>Disadvantage</strong> &nbsp;<span style="color: red;">*</span> </label>
                                                        <textarea class="form-control" rows="4" placeholder="E.g.: escription disadvantage" name="description" id="description"></textarea>
                                                        <p id="error_description" class="text-center text-danger"></p>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="form-row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="first_name"><strong> Image description project </strong>&nbsp;<span style="color: red;">*</span></label>
                                                        <input class="form-control" type="file" placeholder="E.g.: 2 days" name="libelle" id="libelle">
                                                        <p id="error_libelle" class="text-center text-danger" required=""></p>
                                                    </div>
                                                </div>

                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="first_name"><strong> First Document</strong> &nbsp;<span style="color: red;">*</span> </label>
                                                        <input class="form-control" type="file" placeholder="E.g.: 15 days" name="description" id="description">
                                                        <p id="error_description" class="text-center text-danger"></p>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="form-row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="first_name"><strong> Second Document </strong>&nbsp;<span style="color: red;">*</span></label>
                                                        <input class="form-control" type="file" placeholder="E.g.: 2 days" name="libelle" id="libelle">
                                                        <p id="error_libelle" class="text-center text-danger" required=""></p>
                                                    </div>
                                                </div>

                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="first_name"><strong> Thirdly Document</strong> &nbsp;<span style="color: red;">*</span> </label>
                                                        <input class="form-control" type="file" placeholder="E.g.: 15 days" name="description" id="description">
                                                        <p id="error_description" class="text-center text-danger"></p>
                                                    </div>
                                                </div>

                                            </div>


                                            <div class="form-row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="first_name"><strong> Foorty Document </strong>&nbsp;<span style="color: red;">*</span></label>
                                                        <input class="form-control" type="file" placeholder="E.g.: 2 days" name="libelle" id="libelle">
                                                        <p id="error_libelle" class="text-center text-danger" required=""></p>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <button class="btn btn-primary btn-sm" data-bs-hover-animate="pulse" id="add_type_invest" type="button">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                    {{-- End Add Type Pakage --}}

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        {{-- END CONTAINER PAGE --}}



        {{-- Historical Type Invest --}}

                    <div class="container-fluid jello animated">
                <h3 class="text-dark mb-4">Historical Type Investiment (PACK)</h3>
                <div class="card shadow">
                    <div class="card-header pulse animated py-3">
                        <p class="text-primary m-0 font-weight-bold">Employee Info</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                            <table class="table dataTable my-0" id="historicalDataTable">
                                <thead>
                                    <tr class="text-center">
                                        <th id="etat_color" data-field="id">Statut</th>
                                        <th id="id" data-field="id">Id</th>
                                        <th id="user" data-field="user">User</th>
                                        <th id="libelle" data-field="libelle">Libelle</th>
                                        <th id="description" data-field="description">Description</th>
                                        <th id="statut" data-field="etat"> Statut</th>
                                        <th id="created_at" data-field="created_at">Created Date</th>
                                        <th id="action" data-field="action">Action</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                </div>
            </div>


        <div class="modal fade" id="modal-info" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

            <div class="modal-dialog" role="document">

                <div class="modal-content text-center">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">

                        <div class="item form-group">
                            <p id="image_methode"></p>
                        </div>
                        <div class="item form-group">
                            <p class="text-center" style="font-size: 15px;"> Vérification de validation pour le numero :
                                <span id="info_id" style="color: black;"> 000 </span>
                            </p>
                        </div>
                        <br>

                        <div class="item form-group">
                            <p class="text-center" style="font-size: 15px;"> Date de validation :
                                <span id="info_date_val" style="color: black;"> 2019-12-16 </span>
                            </p>
                        </div>
                        <br>

                        <div class="item form-group">
                            <p class="text-center" style="font-size: 15px;"> Dépôt fait par :
                                <span id="info_name" style="color: black;"> Systeme</span>
                            </p>
                        </div>
                        <br>

                        <div class="item form-group">
                            <p class="text-center" style="font-size: 15px;"> Moyen de payement :
                                <span id="info_methode" style="color: black;"> Tmoney </span>
                            </p>
                        </div>
                        <br>

                        <div class="item form-group">
                            <p class="text-center" style="font-size: 15px;"> Montant :
                                <span id="info_montant" style="color: black;"> 000 </span>
                            </p>
                        </div>
                        <br>

                        <div class="item form-group">
                            <p  class="text-center" style="font-size: 15px;"> Réference :
                                <span id="info_reference" style="color: black;"> 000 </span>
                            </p>
                        </div>
                        <br>

                        <div class="item form-group">
                            <p  class="text-center" style="font-size: 15px;"> Etat :
                                <span id="info_etat" style="color: black;">  </span>
                            </p>
                        </div>
                        <br>

                    </div>
                    <div class="modal-footer text-center">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">D'accord</button>
                    </div>
                </div>
            </div>
        </div>

        {{-- End Historical Type Invest --}}

        {{-- End Js Aditionnal --}}


                    <script>
                        $(document).ready(function () {

                            // Onclick Update cfa found 

                            // Begin Updatemin and max CFA
                            $('#add_type_invest').click(function() {                                
                            // Accord de confidentialiter
                                Swal.fire({
                                  title: 'Comfirmation',
                                  text: "Vous êtes sur des informations sur le pack.",
                                  icon: 'question',
                                  showCancelButton: true,
                                  confirmButtonColor: '#3085d6',
                                  cancelButtonColor: '#d33',
                                  confirmButtonText: "Oui, je suis d'accord"
                                }).then((result) => {
                                  if (result.value) {
                                    Swal.fire(
                                      'Tanks',
                                      'Merci pour votre accord.',
                                      'success'
                                    )


                                var data = $('#submit_new_type_invest').serialize();
                                // alert(data);
                                $.ajax({
                                    type: 'post',
                                    data: data,
                                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                                    url: "{{ route('typeInvest.add_type_invest_pack')}}",
                                    success: function (Response) {
                                        console.log(Response);
                                        //alert("success");

                                        /* Initialisation des champs */
                                        document.getElementById("error_libelle").innerHTML = ' ';
                                        document.getElementById("error_description").innerHTML = ' ';


                                        /*validate des champs*/
                                        if (Response.libelle) {
                                            document.getElementById("error_libelle").innerHTML = '<p>' + Response.libelle + '</p>'
                                        }


                                        if (Response.description) {
                                            document.getElementById("error_description").innerHTML = '<p>' + Response.description + '</p>'
                                        }


                                        if(Response.success == "val_ins"){

                                            //var input = document.getElementById("in").value;
                                            var libelEdit = document.getElementById("libelle");
                                            var descriptionEdit = document.getElementById("description");
                                            libelEdit.value = "";
                                            descriptionEdit.value = "";

                                            Swal.fire(
                                              'Effectuer avec success',
                                              'Veuillez.',
                                              'success'
                                            )
                                        }



                                    },

                                })
                                
                                  }
                                })

                                // End Add Type Invest (PACK)

                            });





                        })
                    </script>


                    <script src="{{asset('assets/js/datatable/jquery.dataTables.min.js')}}"></script>


                    <script type="text/javascript">

                        $(function() {
                            // Function pour le chargement des données dans la base de données
                            $('#historicalDataTable').DataTable({
                                processing: true,
                                serverSide: true,
                                destroy :true ,
                                clear : true,
                                paging: true,
                                lengthChange : true,
                                searching : true,
                                ordering : true,
                                autoWidth : true,
                                ajax: '{{ route('sadmin.typeInvest.historicalTypeInvest') }}',
                                columns: [
                                    { data: 'etat_color' , searchable : false},
                                    { data: 'id', name: 'id' },
                                    { data: 'user', name: 'user' },
                                    { data: 'libelle', name: 'libelle' },
                                    { data: 'description', name: 'description' },
                                    { data: 'statut', name: 'statut' },
                                    { data: 'created_at', name: 'created_at' },
                                    { data: 'action' , searchable : false}
                                ]
                            });

                        });


                        // Fonction pour l'affichage du show
                        function showData(id) {
                            $.ajax({
                                url : "{{url('historicalDepositAdmin')}}"+ '/'+ id,
                                type : "GET",
                                dataType : "JSON",
                                success : function (data) {
                                    $('#modal-info').modal('show');
                                    $('.modal-title').text("Détails sur le dépôt de "+ data.user);
                                    $('#info_id').text(data.id);
                                    $('#info_date_val').text(data.created_at);
                                    $('#info_name').text(data.user);
                                    $('#info_montant').text(data.account);
                                    $('#info_reference').text(data.reference);
                                    $('#info_etat').text(data.etat);

                                    //Condiction avancys
                                    
                                    //AU
                                    if(data.method == "AU"){
                                        $('#info_methode').text("Autre Methode");
                                        $("#image_methode").html('<img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="tada" src="{{asset('assets/img/orther.jpg')}}" width="160" height="160">');
                                    }
                                    else{
                                        $('#info_methode').text(data.method);
                                        $("#image_methode").html('<img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="tada" src="{{asset('assets/img/orther.jpg')}}" width="160" height="160">');
                                    }

                                    //Tmoney
                                    if(data.method == "TY"){
                                        $('#info_methode').text("TMONEY");
                                        $("#image_methode").html('<img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="tada" src="{{asset('assets/img/tmoney.jpg')}}" width="160" height="160">');
                                    }

                                    //Flooz
                                    if(data.method == "FZ"){
                                        $('#info_methode').text("FLOOZ");
                                        $("#image_methode").html('<img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="tada" src="{{asset('assets/img/flooz.jpg')}}" width="160" height="160">');
                                    }
                          
                                    //PM          
                                    if(data.method == "PM"){
                                        $('#info_methode').text("Perfect Money");
                                        $("#image_methode").html('<img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="tada" src="{{asset('assets/img/perfectmoney.jpg')}}" width="160" height="160">');
                                    }
                                    
                                    //TICKET
                                    if(data.method == "CD"){
                                        $('#info_methode').text("Code Généré");
                                        $("#image_methode").html('<img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="tada" src="{{asset('assets/img/codegeneration.jpg')}}" width="160" height="160">');
                                    }

                                    


                                },
                                error : function () {
                                    Swal.fire(
                                      'Oops',
                                      'Veuillez réactualiser la page ou connectez-vous',
                                      'warning'
                                    )
                                }
                            });
                        }

                    </script>

@stop




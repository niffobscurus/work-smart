@extends('User.layouts.pagesExtends.formAdminIndex',['title'=> "Deposit"])
@section('main')

	{{-- CONTAINER PAGE --}}

        @if ( strcmp ( $method , "TY" )  == 0 or  strcmp ( $method , "FZ" ) == 0 )

            <div class="container-fluid">
                <h3 class="text-dark mb-4"><a href="#"><i class="fa fa-arrow-left" style="height: 30px;"></i></a>&nbsp; Transaction via Tmoney</h3>
                <div class="row mb-3" style="opacity: 1;">
                    <div class="col-lg-12 flash animated">
                        <div class="card mb-3" style="color: #858796;">
                            <div class="card-body text-center shadow jello animated" style="opacity: 1;color: #858796;">

                                @if ( strcmp ( $method , "TY" )  == 0)
                                    <img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="tada" src="{{asset('assets/img/tmoney.jpg')}}" width="160" height="160">
                                @endif

                                @if (strcmp ( $method , "FZ" )  == 0)
                                    <img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="tada" src="{{asset('assets/img/flooz.jpg')}}" width="160" height="160">
                                @endif

                                @if (strcmp ( $method , "PM" )  == 0)
                                    <img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="tada" src="{{asset('assets/img/perfectmoney.jpg')}}" width="160" height="160">
                                @endif

                                @if (strcmp ( $method , "CD" )  == 0)
                                    <img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="tada" src="{{asset('assets/img/codegeneration.jpg')}}" width="160" height="160">
                                @endif

                                @if (strcmp ( $method , "AU" )  == 0)
                                    <img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="tada" src="{{asset('assets/img/orther.jpg')}}" width="160" height="160">
                                @endif

                                <div class="mb-3"></div>
                                <form method="" id="deposit" role="form">
                                    <div class="form-row d-lg-flex justify-content-lg-center">
                                        <div class="col-lg-6">
                                            <div class="form-group"><label class="d-lg-flex justify-content-lg-start" for="username">Amount</label>
                                                <input class="form-control" type="number" placeholder="user.amount" id="account" name="account" required="" autocomplete="off" minlength="3" maxlength="15">
                                                <p id="error_account" class="text-center text-danger"></p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row text-center d-lg-flex justify-content-lg-center">
                                        <div class="col-lg-6 text-center">
                                            <div class="form-group"><label class="d-lg-flex justify-content-lg-start" for="email">Phone Number</label>
                                                <input class="form-control" type="number" placeholder="use.phoneNumber" id="phone_number" name="phone_number" minlength="8" maxlength="18" required="">
                                                <p id="error_phone_number" class="text-center text-danger"></p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row text-center d-lg-flex justify-content-lg-center">
                                        <div class="col-lg-6 text-center">
                                            <div class="form-group"><label class="d-lg-flex justify-content-lg-start" for="email">Reference</label>
                                                <input class="form-control" type="text" placeholder="use.reference" id="reference" name="reference" minlength="1" maxlength="35" required="">
                                                <p id="error_reference" class="text-center text-danger"></p>
                                                <input class="form-control" type="hidden"  id="method" name="method"  required="" value="{{$method}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class=" custom-switch d-lg-flex justify-content-lg-center">
                                            <input class="custom-control-input" type="checkbox" id="verification" name="verification">
                                            <label class="custom-control-label" for="verification"><strong>Je suis d'accord pour les termes et condictions et d'avoir 18 ans.</strong></label>
                                        </div>
                                        </br><p id="error_verification" class="text-center text-danger"></p>
                                    </div>
                                    <button class="btn btn-primary btn-sm bg-success border-success shadow-lg" data-bs-hover-animate="pulse" id="submit" type="button">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @else

            <div class="container-fluid">
                <h3 class="text-dark mb-4"><a href="#"><i class="fa fa-arrow-left" style="height: 30px;"></i></a>&nbsp; Transaction via Tmoney</h3>
                <div class="row mb-3" style="opacity: 1;">
                    <div class="col-lg-12 flash animated">
                        <div class="card mb-3" style="color: #858796;">
                            <div class="card-body text-center shadow jello animated" style="opacity: 1;color: #858796;">
                                    <h4> Payment Method not exist</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endif




                    <script>
                        $(document).ready(function () {

                            $('#submit').click(function () {

                                // Accord de confidentialiter
                                Swal.fire({
                                  title: 'Terme et accord',
                                  text: "J'accepte les termes du contract et attest avoir 18 ans.",
                                  icon: 'question',
                                  showCancelButton: true,
                                  confirmButtonColor: '#3085d6',
                                  cancelButtonColor: '#d33',
                                  confirmButtonText: "Oui, je suis d'accord"
                                }).then((result) => {
                                  if (result.value) {
                                    Swal.fire(
                                      'Tanks',
                                      'Merci pour votre accord.',
                                      'success'
                                    )


                                var data = $('#deposit').serialize();
                                // alert(data);
                                $.ajax({
                                    type: 'post',
                                    data: data,
                                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                                    url: "{{ route('depositeUser.store')}}",
                                    success: function (Response) {
                                        console.log(Response);
                                        //alert("success");

                                        /* Initialisation des champs */
                                        document.getElementById("error_account").innerHTML = ' ';
                                        document.getElementById("error_phone_number").innerHTML = ' ';
                                        document.getElementById("error_reference").innerHTML = ' ';
                                        document.getElementById("error_verification").innerHTML = ' ';


                                        /*validate des champs*/
                                        if (Response.account) {
                                            document.getElementById("error_account").innerHTML = '<p>' + Response.account + '</p>'
                                        }


                                        if (Response.phone_number) {
                                            document.getElementById("error_phone_number").innerHTML = '<p>' + Response.phone_number + '</p>'
                                        }

                                        if (Response.reference) {
                                            document.getElementById("error_reference").innerHTML = '<p>' + Response.reference + '</p>'
                                        }

                                        if (Response.verification) {
                                            document.getElementById("error_verification").innerHTML = '<p>' + Response.verification + '</p>'
                                        }

                                        if(Response.success == "val_ins"){
                                            Swal.fire({
                                              title: 'Deposits',
                                              text: "Votre depot a été fait avec success",
                                              icon: 'success',
                                              confirmButtonColor: '#3085d6',
                                              cancelButtonColor: '#d33',
                                              confirmButtonText: 'D\'accord'
                                            }).then((result) => {
                                              if (result.value) {
                                                Swal.fire(
                                                  'Deposits',
                                                  'Effectuer avec succes.',
                                                  'success'
                                                )
                                              }
                                                window.location.href = '/depositeUser';
                                            })
                                            
                                        }



                                    },

                                })
                                  }
                                })


                            });

                        })
                    </script>

                    {{-- End  --}}


{{-- END CONTAINER PAGE --}}

@stop




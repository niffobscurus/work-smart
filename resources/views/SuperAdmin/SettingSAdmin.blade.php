@extends('SuperAdmin.layouts.pagesExtends.formAdminIndex',['title'=>' Setting'])
@section('main')

	{{-- CONTAINER PAGE --}}


            <div class="container-fluid">
                <h3 class="text-dark mb-4">Profile</h3>
                <div class="row mb-3">
                    <div class="col-lg-4 flash animated">
                        <div class="card mb-3">
                            <div class="card-body text-center shadow"><img class="rounded-circle mb-3 mt-4" data-aos="zoom-in-right" data-aos-duration="850" src="assets/img/dogs/image2.jpeg" width="160" height="160">
                                <div class="mb-3"><button class="btn btn-primary btn-sm" type="button">Change Photo</button></div>
                            </div>
                        </div>
                        <div class="card shadow mb-4" data-aos="zoom-out-down" data-aos-duration="1250">
                            <div class="card-header py-3">
                                <h6 class="text-primary font-weight-bold m-0">Projects</h6>
                            </div>
                            <div class="card-body">
                                <h4 class="small font-weight-bold">Server migration<span class="float-right">20%</span></h4>
                                <div class="progress progress-sm mb-3">
                                    <div class="progress-bar bg-danger" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%;"><span class="sr-only">20%</span></div>
                                </div>
                                <h4 class="small font-weight-bold">Sales tracking<span class="float-right">40%</span></h4>
                                <div class="progress progress-sm mb-3">
                                    <div class="progress-bar bg-warning" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;"><span class="sr-only">40%</span></div>
                                </div>
                                <h4 class="small font-weight-bold">Customer Database<span class="float-right">60%</span></h4>
                                <div class="progress progress-sm mb-3">
                                    <div class="progress-bar bg-primary" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"><span class="sr-only">60%</span></div>
                                </div>
                                <h4 class="small font-weight-bold">Payout Details<span class="float-right">80%</span></h4>
                                <div class="progress progress-sm mb-3">
                                    <div class="progress-bar bg-info" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;"><span class="sr-only">80%</span></div>
                                </div>
                                <h4 class="small font-weight-bold">Account setup<span class="float-right">Complete!</span></h4>
                                <div class="progress progress-sm mb-3">
                                    <div class="progress-bar bg-success" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"><span class="sr-only">100%</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="row mb-3 d-none">
                            <div class="col">
                                <div class="card text-white bg-primary shadow">
                                    <div class="card-body">
                                        <div class="row mb-2">
                                            <div class="col">
                                                <p class="m-0">Peformance</p>
                                                <p class="m-0"><strong>65.2%</strong></p>
                                            </div>
                                            <div class="col-auto"><i class="fas fa-rocket fa-2x"></i></div>
                                        </div>
                                        <p class="text-white-50 small m-0"><i class="fas fa-arrow-up"></i>&nbsp;5% since last month</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="card text-white bg-success shadow">
                                    <div class="card-body">
                                        <div class="row mb-2">
                                            <div class="col">
                                                <p class="m-0">Peformance</p>
                                                <p class="m-0"><strong>65.2%</strong></p>
                                            </div>
                                            <div class="col-auto"><i class="fas fa-rocket fa-2x"></i></div>
                                        </div>
                                        <p class="text-white-50 small m-0"><i class="fas fa-arrow-up"></i>&nbsp;5% since last month</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- Deposite Configuration --}}
                        <div class="row pulse animated">
                            <div class="col pulse animated">
                                <div class="card shadow mb-3" data-aos="flip-left" data-aos-duration="750">
                                    <div class="card-header py-3">
                                        <p class="text-primary m-0 font-weight-bold">Settings Deposits C.F.A.</p>
                                    </div>

                                    {{-- Begin Update CFA VAlue --}}
                                    <div class="card-body">
                                        <form id="update_cfa_main" method="post">
                                            <div class="form-row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="first_name"><strong>Minimum Deposits</strong></label>
                                                        <input class="form-control" type="number" placeholder="{{ $value_cfa->min_deposit_cfa }}" value="{{ $value_cfa->min_deposit_cfa }}"  name="min_deposit_cfa" id="min_deposit_cfa">
                                                        <p id="error_deposit_min" class="text-center text-danger"></p>
                                                    </div>
                                                </div>

                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="last_name"><strong>Maximum Deposits</strong></label>
                                                        <input class="form-control" type="number" placeholder="{{ $value_cfa->maw_deposit_cfa }}" value="{{ $value_cfa->maw_deposit_cfa }}" name="maw_deposit_cfa" id="maw_deposit_cfa">
                                                        <p id="error_deposit_max" class="text-center text-danger"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary btn-sm" data-bs-hover-animate="pulse" id="update_cfa" type="button">Update Value</button>
                                            </div>
                                        </form>
                                    </div>
                                    {{-- End Update CFA VAlue --}}

                                </div>

                                <div class="card shadow mb-3" data-aos="flip-left" data-aos-duration="750">
                                    <div class="card-header py-3">
                                        <p class="text-primary m-0 font-weight-bold">Settings Deposits Dollar</p>
                                    </div>

                                    {{-- Begin Update Dollar VAlue --}}
                                    <div class="card-body">
                                        <form id="update_dollar_main" method="post">
                                            <div class="form-row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="first_name"><strong>Minimum Deposits</strong></label>
                                                        <input class="form-control" type="number" placeholder="{{ $value_cfa->min_deposit_dollar }}" value="{{ $value_cfa->min_deposit_dollar }}"  name="min_deposit_dollar" id="min_deposit_dollar">
                                                        <p id="error_deposit_dollar_min" class="text-center text-danger"></p>
                                                    </div>
                                                </div>

                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="last_name"><strong>Maximum Deposits</strong></label>
                                                        <input class="form-control" type="number" placeholder="{{ $value_cfa->max_deposit_dollar }}" value="{{ $value_cfa->max_deposit_dollar }}" name="max_deposit_dollar" id="max_deposit_dollar">
                                                        <p id="error_deposit_dollar_max" class="text-center text-danger"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary btn-sm" data-bs-hover-animate="pulse" id="update_dollar" type="button">Update Value</button>
                                            </div>
                                        </form>
                                    </div>
                                    {{-- End Update Dollar VAlue --}}

                                </div>
                            </div>
                        </div>

                        {{-- withdrawal Configuration --}}
                        <div class="row pulse animated">
                            <div class="col pulse animated">
                                <div class="card shadow mb-3" data-aos="flip-left" data-aos-duration="750">
                                    <div class="card-header py-3">
                                        <p class="text-primary m-0 font-weight-bold">Settings Withdrawal C.F.A.</p>
                                    </div>

                                    {{-- Begin Update CFA VAlue --}}
                                    <div class="card-body">
                                        <form id="update_withdrawal_cfa_main" method="post">
                                            <div class="form-row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="first_name"><strong>Minimum Deposits</strong></label>
                                                        <input class="form-control" type="number" placeholder="{{ $value_cfa->min_withdrawal_cfa }}" value="{{ $value_cfa->min_withdrawal_cfa }}"  name="min_withdrawal_cfa" id="min_withdrawal_cfa">
                                                        <p id="error_withdrawal_min" class="text-center text-danger"></p>
                                                    </div>
                                                </div>

                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="last_name"><strong>Maximum Deposits</strong></label>
                                                        <input class="form-control" type="number" placeholder="{{ $value_cfa->max_withdrawal_cfa }}" value="{{ $value_cfa->max_withdrawal_cfa }}" name="max_withdrawal_cfa" id="max_withdrawal_cfa">
                                                        <p id="error_withdrawal_max" class="text-center text-danger"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary btn-sm" data-bs-hover-animate="pulse" id="update_withdrawal_cfa" type="button">Update Value</button>
                                            </div>
                                        </form>
                                    </div>
                                    {{-- End Update CFA VAlue --}}

                                </div>

                                <div class="card shadow mb-3" data-aos="flip-left" data-aos-duration="750">
                                    <div class="card-header py-3">
                                        <p class="text-primary m-0 font-weight-bold">Settings Withdrawal Dollar</p>
                                    </div>

                                    {{-- Begin Update Dollar VAlue --}}
                                    <div class="card-body">
                                        <form id="update_withdrawal_dollar_main" method="post">
                                            <div class="form-row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="first_name"><strong>Minimum Deposits</strong></label>
                                                        <input class="form-control" type="number" placeholder="{{ $value_cfa->min_withdrawal_dollar }}" value="{{ $value_cfa->min_withdrawal_dollar }}"  name="min_withdrawal_dollar" id="min_withdrawal_dollar">
                                                        <p id="error_withdrawal_dollar_min" class="text-center text-danger"></p>
                                                    </div>
                                                </div>

                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="last_name"><strong>Maximum Deposits</strong></label>
                                                        <input class="form-control" type="number" placeholder="{{ $value_cfa->max_withdrawal_dollar }}" value="{{ $value_cfa->max_withdrawal_dollar }}" name="max_withdrawal_dollar" id="max_withdrawal_dollar">
                                                        <p id="error_withdrawal_dollar_max" class="text-center text-danger"></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <button class="btn btn-primary btn-sm" data-bs-hover-animate="pulse" id="update_withdrawal_dollar" type="button">Update Value</button>
                                            </div>
                                        </form>
                                    </div>
                                    {{-- End Update Dollar VAlue --}}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        {{-- END CONTAINER PAGE --}}

        {{-- End Js Aditionnal --}}


                    <script>
                        $(document).ready(function () {

                            // Onclick Update cfa found

                            // Begin Updatemin and max CFA
                            $('#update_cfa').click(function() {                                
                            // Accord de confidentialiter
                                Swal.fire({
                                  title: 'Comfirmation',
                                  text: "Vous êtes sur des valeurs.",
                                  icon: 'question',
                                  showCancelButton: true,
                                  confirmButtonColor: '#3085d6',
                                  cancelButtonColor: '#d33',
                                  confirmButtonText: "Oui, je suis d'accord"
                                }).then((result) => {
                                  if (result.value) {
                                    Swal.fire(
                                      'Tanks',
                                      'Merci pour votre accord.',
                                      'success'
                                    )


                                var data = $('#update_cfa_main').serialize();
                                // alert(data);
                                $.ajax({
                                    type: 'post',
                                    data: data,
                                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                                    url: "{{ route('settingSAdmin.update_cfa')}}",
                                    success: function (Response) {
                                        console.log(Response);
                                        //alert("success");

                                        /* Initialisation des champs */
                                        document.getElementById("error_deposit_min").innerHTML = ' ';
                                        document.getElementById("error_deposit_max").innerHTML = ' ';


                                        /*validate des champs*/
                                        if (Response.min_deposit_cfa) {
                                            document.getElementById("error_deposit_min").innerHTML = '<p>' + Response.min_deposit_cfa + '</p>'
                                        }


                                        if (Response.maw_deposit_cfa) {
                                            document.getElementById("error_deposit_max").innerHTML = '<p>' + Response.maw_deposit_cfa + '</p>'
                                        }


                                        if(Response.success == "val_ins"){
                                            Swal.fire(
                                              'Effectuer avec success',
                                              'Veuillez.',
                                              'success'
                                            )
                                        }



                                    },

                                })
                                
                                  }
                                })

                                // End Updatemin and max CFA

                            });


                            // Begin Updatemin and max CFA
                            $('#update_dollar').click(function() {                                
                            // Accord de confidentialiter
                                Swal.fire({
                                  title: 'Comfirmation',
                                  text: "Vous êtes sur des valeurs.",
                                  icon: 'question',
                                  showCancelButton: true,
                                  confirmButtonColor: '#3085d6',
                                  cancelButtonColor: '#d33',
                                  confirmButtonText: "Oui, je suis d'accord"
                                }).then((result) => {
                                  if (result.value) {
                                    Swal.fire(
                                      'Tanks',
                                      'Merci pour votre accord.',
                                      'success'
                                    )


                                var data = $('#update_dollar_main').serialize();
                                // alert(data);
                                $.ajax({
                                    type: 'post',
                                    data: data,
                                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                                    url: "{{ route('settingSAdmin.update_dollar')}}",
                                    success: function (Response) {
                                        console.log(Response);
                                        //alert("success");

                                        /* Initialisation des champs */
                                        document.getElementById("error_deposit_dollar_min").innerHTML = ' ';
                                        document.getElementById("error_deposit_dollar_max").innerHTML = ' ';


                                        /*validate des champs*/
                                        if (Response.min_deposit_dollar) {
                                            document.getElementById("error_deposit_dollar_min").innerHTML = '<p>' + Response.min_deposit_dollar + '</p>'
                                        }


                                        if (Response.max_deposit_dollar) {
                                            document.getElementById("error_deposit_dollar_max").innerHTML = '<p>' + Response.max_deposit_dollar + '</p>'
                                        }


                                        if(Response.success == "val_ins"){
                                            Swal.fire(
                                              'Effectuer avec success',
                                              'Veuillez.',
                                              'success'
                                            )
                                        }



                                    },

                                })
                                
                                  }
                                })

                                // End Updatemin and max CFA

                            });

                            /*******************************/
                            /*  */
                            /******************************/

                            // Begin Updatemin and max CFA
                            $('#update_withdrawal_cfa').click(function() {                                
                            // Accord de confidentialiter
                                Swal.fire({
                                  title: 'Comfirmation',
                                  text: "Vous êtes sur des valeurs.",
                                  icon: 'question',
                                  showCancelButton: true,
                                  confirmButtonColor: '#3085d6',
                                  cancelButtonColor: '#d33',
                                  confirmButtonText: "Oui, je suis d'accord"
                                }).then((result) => {
                                  if (result.value) {
                                    Swal.fire(
                                      'Tanks',
                                      'Merci pour votre accord.',
                                      'success'
                                    )


                                var data = $('#update_withdrawal_cfa_main').serialize();
                                // alert(data);
                                $.ajax({
                                    type: 'post',
                                    data: data,
                                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                                    url: "{{ route('settingSAdmin.update_withdrawal_cfa')}}",
                                    success: function (Response) {
                                        console.log(Response);
                                        //alert("success");

                                        /* Initialisation des champs */
                                        document.getElementById("error_withdrawal_min").innerHTML = ' ';
                                        document.getElementById("error_withdrawal_max").innerHTML = ' ';


                                        /*validate des champs*/
                                        if (Response.min_deposit_cfa) {
                                            document.getElementById("error_withdrawal_min").innerHTML = '<p>' + Response.min_withdrawal_cfa + '</p>'
                                        }


                                        if (Response.maw_deposit_cfa) {
                                            document.getElementById("error_withdrawal_max").innerHTML = '<p>' + Response.max_withdrawal_cfa + '</p>'
                                        }


                                        if(Response.success == "val_ins"){
                                            Swal.fire(
                                              'Effectuer avec success',
                                              'Veuillez.',
                                              'success'
                                            )
                                        }



                                    },

                                })
                                
                                  }
                                })

                                // End Updatemin and max CFA

                            });


                            // Begin Updatemin and max CFA
                            $('#update_withdrawal_dollar').click(function() {                                
                            // Accord de confidentialiter
                                Swal.fire({
                                  title: 'Comfirmation',
                                  text: "Vous êtes sur des valeurs.",
                                  icon: 'question',
                                  showCancelButton: true,
                                  confirmButtonColor: '#3085d6',
                                  cancelButtonColor: '#d33',
                                  confirmButtonText: "Oui, je suis d'accord"
                                }).then((result) => {
                                  if (result.value) {
                                    Swal.fire(
                                      'Tanks',
                                      'Merci pour votre accord.',
                                      'success'
                                    )


                                var data = $('#update_withdrawal_dollar_main').serialize();
                                // alert(data);
                                $.ajax({
                                    type: 'post',
                                    data: data,
                                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                                    url: "{{ route('settingSAdmin.update_withdrawal_dollar')}}",
                                    success: function (Response) {
                                        console.log(Response);
                                        //alert("success");

                                        /* Initialisation des champs */
                                        document.getElementById("error_withdrawal_dollar_min").innerHTML = ' ';
                                        document.getElementById("error_withdrawal_dollar_max").innerHTML = ' ';


                                        /*validate des champs*/
                                        if (Response.min_deposit_dollar) {
                                            document.getElementById("error_withdrawal_dollar_min").innerHTML = '<p>' + Response.min_withdrawal_dollar + '</p>'
                                        }


                                        if (Response.max_deposit_dollar) {
                                            document.getElementById("error_withdrawal_dollar_max").innerHTML = '<p>' + Response.max_withdrawal_dollar + '</p>'
                                        }


                                        if(Response.success == "val_ins"){
                                            Swal.fire(
                                              'Effectuer avec success',
                                              'Veuillez.',
                                              'success'
                                            )
                                        }



                                    },

                                })
                                
                                  }
                                })

                                // End Updatemin and max CFA

                            });


                        })
                    </script>

@stop





<nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0">
    <div class="container-fluid d-flex flex-column p-0">
        <a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="#">
            <div class="sidebar-brand-icon rotate-n-15"><i class="fas fa-laugh-wink"></i></div>
            <div class="sidebar-brand-text mx-3"><span>Brand</span></div>
        </a>
        <hr class="sidebar-divider my-0">
        <ul class="nav navbar-nav text-light" id="accordionSidebar">
            <li class="nav-item" role="presentation"><a class="nav-link active" href="/homeUser"><i class="fas fa-tachometer-alt"></i><span>Dashboard</span></a></li>
            <li class="nav-item" role="presentation"></li>
            <li class="nav-item" role="presentation"><a class="nav-link" href="/balanceUser"><i class="fas fa-money-bill-wave"></i><span>Balance</span></a></li>
            <li class="nav-item" role="presentation"><a class="nav-link" href="/depositeUser"><i class="fas fa-money-check"></i><span>Deposits</span></a></li>
            <li class="nav-item" role="presentation"><a class="nav-link" href="/withdrawalUser"><i class="fas fa-arrow-up"></i><span></span>Withdrawals</a></li>
            <li class="nav-item" role="presentation">
                <a class="nav-link" href="table.html"><i class="fas fa-table"></i>&nbsp;Investments in progress</a>
                <a class="nav-link" href="invest_past.html"><i class="fas fa-table"></i><span>Historicals past investments&nbsp;</span></a>
                <a class="nav-link" href="table.html"><i class="fas fa-table"></i><span>&nbsp;Deposit history</span></a>
                <a class="nav-link" href="table.html"><i class="fas fa-table"></i><span>&nbsp;Withdrawal history</span></a>
            </li>
            <li class="nav-item" role="presentation"><a class="nav-link" href="/typeInvestSAdmin"><i class="fas fa-user"></i><span>Type PAck Investiment</span></a></li>
            <li class="nav-item" role="presentation"><a class="nav-link" href="/investSAdmin"><i class="fas fa-user"></i><span>Pack Investiment</span></a></li>
            <li class="nav-item" role="presentation"><a class="nav-link" href="/settingSAdmin"><i class="fas fa-user"></i><span>Setting</span></a></li>
            <li class="nav-item" role="presentation"><a class="nav-link" href="/profileUser"><i class="fas fa-user"></i><span>Profile</span></a></li>
        </ul>
        <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
    </div>
</nav>
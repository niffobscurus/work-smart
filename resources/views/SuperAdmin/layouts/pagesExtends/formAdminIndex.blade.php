<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Invest : {{ $title }}</title>
    <meta name="csrf_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">

    {{-- CSS --}}
    @include('SuperAdmin.layouts.partials._css_main')

    {{-- END CSS --}}

</head>

<body id="page-top">
    <div id="wrapper">

        {{-- MAIN MENU --}}
        @include('SuperAdmin.layouts.partials._menu_main')
        {{-- END MAIN MENU --}}

        <div class="d-flex flex-column" id="content-wrapper">

            {{-- BAR NOTIFICATION MENU --}}
            @include('SuperAdmin.layouts.partials._notification_bar')
            {{-- END BAR NOTIFICATION MENU --}}

            </nav>

            {{-- CONTAINER PAGE --}}
            @yield('main')
            {{-- END CONTAINER PAGE --}}

        </div>

            {{-- FOOTER --}}
            @include('SuperAdmin.layouts.partials._footer')
            {{-- END FOOTER --}}

    </div>



    {{-- JS  --}}
     @include('SuperAdmin.layouts.partials._js_main')
    {{-- END JS  --}}

</body>

</html>
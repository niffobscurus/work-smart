@extends('User.layouts.pagesExtends.formAdminIndex',['title'=>' Withdrawal'])
@section('main')

{{-- CONTAINER PAGE --}}

            <div class="container-fluid">
                <h3 class="text-dark mb-4"><strong>Deposits</strong></h3>
                <div class="row">
                    <div class="col-lg-3 offset-lg-0 rubberBand animated">
                        <div class="card shadow mb-4"></div>
                        <div class="card mb-5">
                            <div class="card-body text-center shadow" data-bs-hover-animate="tada">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col text-right col-md-12">
                                            <p class="text-center float-right col-md-12">Dépôt sur&nbsp;compte&nbsp;FCFA</p>
                                        </div>
                                    </div>
                                </div><a class="card-link" href="detail_payement.html"><img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="swing" src="assets/img/tmoney_one.jpg" width="160" height="160"></a>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col text-right col-md-12">
                                            <p class="text-center float-right col-md-12">Dépôt via Tmoney</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3" style="margin-top: 5%;"><a href="detail_payement.html"><button class="btn btn-success btn-sm" data-bs-hover-animate="pulse" type="button">Make deposit</button></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 offset-lg-0 rubberBand animated">
                        <div class="card shadow mb-4"></div>
                        <div class="card mb-5">
                            <div class="card-body text-center shadow" data-bs-hover-animate="tada">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col text-right col-md-12">
                                            <p class="text-center float-right col-md-12">Dépôt sur compte FCFA</p>
                                        </div>
                                    </div>
                                </div><a class="card-link" href="#"><img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="swing" src="assets/img/flooz_one.jpg" width="160" height="160"></a>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col text-right col-md-12">
                                            <p class="text-center float-right col-md-12">Dépôt via Flooz</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3" style="margin-top: 5%;"><a href="#"><button class="btn btn-success btn-sm" data-bs-hover-animate="pulse" type="button">Make deposit</button></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 offset-lg-0 rubberBand animated">
                        <div class="card shadow mb-4"></div>
                        <div class="card mb-5">
                            <div class="card-body text-center shadow" data-bs-hover-animate="tada">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col text-right col-md-12">
                                            <p class="text-center float-right col-md-12">Dépôt sur Compte Dollar</p>
                                        </div>
                                    </div>
                                </div><a class="card-link" href="#"><img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="swing" src="assets/img/pm_one.jpg" width="160" height="160"></a>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col text-right col-md-12">
                                            <p class="text-center float-right col-md-12">Dépôt via Perfect Money</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3" style="margin-top: 5%;"><a href="#"><button class="btn btn-success btn-sm" data-bs-hover-animate="pulse" type="button">Make deposit</button></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 offset-lg-0 rubberBand animated">
                        <div class="card shadow mb-4"></div>
                        <div class="card mb-5">
                            <div class="card-body text-center shadow" data-bs-hover-animate="tada">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col text-right col-md-12">
                                            <p class="text-center float-right col-md-12">Dépôt sur FCFA ou Dollar</p>
                                        </div>
                                    </div>
                                </div><a class="card-link" href="#"><img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="swing" src="assets/img/saisir_code_one.jpg" width="160" height="160"></a>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col text-right col-md-12">
                                            <p class="text-center float-right col-md-12">Dépôt via Perfect Money</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3" style="margin-top: 5%;"><a href="#"><button class="btn btn-success btn-sm" data-bs-hover-animate="pulse" type="button">Make deposit</button></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 offset-lg-0 rubberBand animated">
                        <div class="card shadow mb-4"></div>
                        <div class="card mb-5">
                            <div class="card-body text-center shadow" data-bs-hover-animate="tada">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col text-right col-md-12">
                                            <p class="text-center float-right col-md-12">Dépôt sur compte FCFA</p>
                                        </div>
                                    </div>
                                </div><a class="card-link" href="#"><img class="rounded-circle mb-3 mt-4" data-bs-hover-animate="swing" src="assets/img/autre_one.jpg" width="160" height="160"></a>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col text-right col-md-12">
                                            <p class="text-center float-right col-md-12">Dépôt via Perfect Money</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3" style="margin-top: 5%;"><a href="#"><button class="btn btn-success btn-sm" data-bs-hover-animate="pulse" type="button">Make deposit</button></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



{{-- END CONTAINER PAGE --}}

@stop




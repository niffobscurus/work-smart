<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="csrf_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{asset('build/css/intlTelInput.min.css')}}">
    <title>Invest : {{ $title }}</title>

    {{-- CSS --}}
    @include('layouts.partials._css_main')

    {{-- END CSS --}}

</head>

<body class="bg-gradient-primary">
    
    {{-- CONTAINER PAGE --}}
     @yield('main')
    {{-- END CONTAINER PAGE --}}

    {{-- JS  --}}
     @include('layouts.partials._js_main')
    {{-- END JS  --}}

</body>

</html>
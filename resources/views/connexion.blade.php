@extends('layouts.pagesExtends.formAdminIndex',['title'=>' Connexion'])
@section('main')

	{{-- CONTAINER PAGE --}}


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9 col-lg-12 col-xl-10">
                <div class="card shadow-lg o-hidden border-0 my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-flex">
                                <div class="flex-grow-1 bg-login-image" style="background-image: url(&quot;assets/img//nami_invest.jpg&quot;);"></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h4 class="text-dark mb-4">Welcome Back!</h4>
                                    </div>
                                    <form class="user">
                                        <div class="form-group">
                                            <input class="form-control form-control-user" type="text" id="user" aria-describedby="emailHelp" placeholder="Pseudo or phone number or email" name="user">
                                            <p id="error_user" class="text-center"></p>
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control form-control-user" type="password" id="password" placeholder="Password" name="password">
                                            <p id="error_password" class="text-center"></p>
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <div class="form-check">
                                                    <input class="form-check-input custom-control-input" type="checkbox" id="verification" name="verification">
                                                    <label class="form-check-label custom-control-label" for="formCheck-1">Remember Me</label>
                                                </div>
                                            </div>
                                        </div>
                                        <button class="btn btn-primary btn-block text-white btn-user" id="valider" type="button">Login</button>
                                        <hr>
                                        <a class="btn btn-primary btn-block text-white btn-google btn-user" role="button">
                                            <i class="fab fa-google"></i>&nbsp; Login with Google</a>
                                            <a class="btn btn-primary btn-block text-white btn-facebook btn-user" role="button">
                                                <i class="fab fa-facebook-f"></i>&nbsp; Login with Facebook</a>
                                        <hr>
                                    </form>

                                    <div class="text-center"><a class="small" href="forgot-password.html">Forgot Password?</a></div>
                                    <div class="text-center"><a class="small" href="/register">Create an Account!</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


            <div class="col-sm-6">
                <script type="text/javascript">
                    jQuery(document).ready(function ($) {

    // Validation and Ajax action
                        $("#valider").click(function (){

    // Form Processing via AJAX
                      

                                $.ajax({
                                    url: "{{ route('connexion.store')}}",
                                    method: 'POST',
                                    dataType: 'json',
                                    "headers": {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content')},
                                    data: {
                                        do_login: true,
                                        user: $('input[name=user]').val(),
                                        password: $('input[name=password]').val(),
                                    },
                                    success: function (Response) {
                                        /* Initialisation des champs */
                                        //console.log("dfdsf");

                                        document.getElementById("error_user").innerHTML = ' ';

                                        /*validate des champs*/
                                        if (Response.user) {
                                            document.getElementById("error_user").innerHTML = '<p>' + Response.user + '</p>'
                                        }

                                        if (Response.success =="con_user") {
                                            window.location.href = '/homeUser';
                                        }

                                        if (Response.success =="con_super_admin") {
                                            window.location.href = '/homeSAdmin';
                                        }

                                        if (Response.success =="con_admin") {
                                            window.location.href = '/homeAdmin';
                                        }

                                        if (Response.success =="con_user_email") {
                                           Swal.fire({
                                              icon: 'error',
                                              title: 'Oops...',
                                              text: 'Veuillez valider votre compte!',
                                              footer: '<a href> comment validez son compte? </a>'
                                            })
                                        }

                                        if (Response.success == "Impo_AD" || Response.success == "Impo_U") {
                                            Swal.fire(
                                              'Information!',
                                              'Votre identifiant ou mot de passe est incorrect',
                                              'warning'
                                            )
                                        }



                                        //window.location.href = '/adminacceuil';
                                        /*show_loading_bar({
                                        delay: .5,
                                        pct: 100,
                                            finish: function(){
                                                // Redirect after successful login page (when progress bar reaches 100%)
                                                if(resp.accessGranted)
                                                    {
                                                    window.location.href = '/welcome';
                                                    }
                                                else
                                                    {
                                                    toastr.error("You have entered wrong password, please try again. User and password is <strong>demo/demo</strong> :)", "Invalid Login!", opts);
                                                    $(form).find('#passwd').select();
                                                    }
                                                }
                                            });*/
                                    }

                                });
                            
                        });
    // Set Form focus
                        $("form#login .form-group:has(.form-control):first .form-control").focus();
                    });
                </script>

                {{-- Importation des plugin supplementaire --}}

                <script id="script-resource-7" src="{{asset('assets/js/jquery-validate/jquery.validate.min.js')}}">

{{-- END CONTAINER PAGE --}}

@stop



